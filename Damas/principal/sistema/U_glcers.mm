//---------------------------------------------------------------------------

#include "U_glcers.h"
//---------------------------------------------------------------------------
// classe Tp_luz
//---------------------------------------------------------------------------

#pragma mark - classe Tp_luz
#pragma mark -

Tp_luz::~Tp_luz()
{
  glDisable(GL_LIGHT0 + indx_da_luz);
}
//---------------------------------------------------------------------------

void Tp_luz::desenha()
{
  glLightfv(GL_LIGHT0 + indx_da_luz, GL_DIFFUSE, cor_difusa);
  glLightfv(GL_LIGHT0 + indx_da_luz, GL_POSITION, pos_da_luz);
  glLightfv(GL_LIGHT0 + indx_da_luz, GL_SPECULAR, cor_especular);
}
//---------------------------------------------------------------------------

void Tp_luz::set_pos_da_luz(const float x, const float y, const float z, const float a)
{
  pos_da_luz[0] = x; pos_da_luz[1] = y; pos_da_luz[2] = z; pos_da_luz[3] = a;
}
//---------------------------------------------------------------------------

void Tp_luz::set_cor_difusa(const float r, const float g, const float b, const float a)
{
  cor_difusa[0] = r; cor_difusa[1] = g; cor_difusa[2] = b; cor_difusa[3] = a;
}
//---------------------------------------------------------------------------

void Tp_luz::set_cor_especular(const float r, const float g, const float b, const float a)
{
  cor_especular[0] = r; cor_especular[1] = g; cor_especular[2] = b; cor_especular[3] = a;
}
//---------------------------------------------------------------------------

Tp_luz::Tp_luz(const int _indx_da_luz, const float _pos_da_luz[], const float _cor_difusa[], const float _cor_especular[])
{
  indx_da_luz = _indx_da_luz;

  for (int i = 0; i < 4; i++)
  {
    pos_da_luz[i] = _pos_da_luz[i];
    cor_difusa[i] = _cor_difusa[i];
    cor_especular[i] = _cor_especular[i];
  }

  if (! (glIsEnabled(GL_LIGHTING))) glEnable(GL_LIGHTING); glEnable(GL_LIGHT0 + indx_da_luz);
}
//---------------------------------------------------------------------------
// classe Tp_vrt
//---------------------------------------------------------------------------

#pragma mark - classe Tp_vrt
#pragma mark -

Tp_vrt::Tp_vrt()
{
  x = y = z = 0.0f;
}
//---------------------------------------------------------------------------

Tp_vrt::~Tp_vrt(){}
//---------------------------------------------------------------------------

Tp_vrt::Tp_vrt(const float _x, const float _y, const float _z)
{
  x = _x; y = _y; z = _z;
}
//---------------------------------------------------------------------------
// classe Tp_camera
//---------------------------------------------------------------------------

#pragma mark - classe Tp_camera
#pragma mark -

Tp_camera::Tp_camera()
{
  passo = 1.0f;
  ang_rot = 0.75f;
  vetor_up = vu_z;
  valv = new Tp_vrt(0.0f, 0.0f, 0.0f);
  vobs = new Tp_vrt(0.0f, -100.0f, 10.0f);
  valv_temp = new Tp_vrt(0.0f, 0.0f, 0.0f);
  vobs_temp = new Tp_vrt(0.0f, -100.0f, 0.0f);
  press_w = press_s = press_t = press_g =
  press_up = press_down = press_left = press_right = false;
}
//---------------------------------------------------------------------------

Tp_camera::~Tp_camera()
{
  delete valv; valv = NULL;
  delete vobs; vobs = NULL;
  delete valv_temp; valv_temp = NULL;
  delete vobs_temp; vobs_temp = NULL;
}
//---------------------------------------------------------------------------

void Tp_camera::trabalha()
{
  switch (vetor_up)
  {
    case vu_y :
      if (press_w) vobs -> y -= passo;
      if (press_s) vobs -> y += passo;
      if (press_t){ valv -> y += passo; vobs -> y += passo;}
      if (press_g){ valv -> y -= passo; vobs -> y -= passo;}
      if (press_up)
      {
        float xpasso = vobs_temp -> x - valv_temp -> x;
        float zpasso = vobs_temp -> z - valv_temp -> z;
        vobs -> x += xpasso; valv -> x += xpasso; vobs -> z += zpasso; valv -> z += zpasso;
      }
      if (press_down)
      {
        float xpasso = vobs_temp -> x - valv_temp -> x;
        float zpasso = vobs_temp -> z - valv_temp -> z;
        vobs -> x -= xpasso; valv -> x -= xpasso; vobs -> z -= zpasso; valv -> z -= zpasso;
      }
      if (press_left){ rotaciona_no_eixo_y(valv, vobs, -ang_rot); rotaciona_no_eixo_y(valv_temp, vobs_temp, -ang_rot);}
      if (press_right){ rotaciona_no_eixo_y(valv, vobs,  ang_rot); rotaciona_no_eixo_y(valv_temp, vobs_temp,  ang_rot);}
    break;
    case vu_z :
      if (press_w) vobs -> z -= passo;
      if (press_s) vobs -> z += passo;
      if (press_t){ valv -> z += passo; vobs -> z += passo;}
      if (press_g){ valv -> z -= passo; vobs -> z -= passo;}
      if (press_up)
      {
        float xpasso = vobs_temp -> x - valv_temp -> x;
        float ypasso = vobs_temp -> y - valv_temp -> y;
        vobs -> x -= xpasso; valv -> x -= xpasso; vobs -> y -= ypasso; valv -> y -= ypasso;
      }
      if (press_down)
      {
        float xpasso = vobs_temp -> x - valv_temp -> x;
        float ypasso = vobs_temp -> y - valv_temp -> y;
        vobs -> x += xpasso; valv -> x += xpasso; vobs -> y += ypasso; valv -> y += ypasso;
      }
      if (press_left){ rotaciona_no_eixo_z(valv, vobs, -ang_rot); rotaciona_no_eixo_z(valv_temp, vobs_temp, -ang_rot);}
      if (press_right){ rotaciona_no_eixo_z(valv, vobs,  ang_rot); rotaciona_no_eixo_z(valv_temp, vobs_temp,  ang_rot);}
    break;
    default : break;
  }
}
//---------------------------------------------------------------------------

void Tp_camera::rotaciona_no_eixo_x(Tp_vrt *vpiv, Tp_vrt *vmov, const float ang)
{
  Tp_vrt vtmp;
  float arad = ang * M_PI / 180.0f;
  calcula_distancia_dos_vertices_em_relacao_ao_pivot(vpiv, vmov, &vtmp);

  vmov -> y = (vpiv -> y + (vtmp.y * cos(arad) + vtmp.z * sin(arad)));
  vmov -> z = (vpiv -> z + (-vtmp.y * sin(arad) + vtmp.z * cos(arad)));
}
//---------------------------------------------------------------------------

void Tp_camera::rotaciona_no_eixo_y(Tp_vrt *vpiv, Tp_vrt *vmov, const float ang)
{
  Tp_vrt vtmp;
  float arad = ang * M_PI / 180.0f;
  calcula_distancia_dos_vertices_em_relacao_ao_pivot(vpiv, vmov, &vtmp);

  vmov -> x = (vpiv -> x + (vtmp.x * cos(arad) - vtmp.z * sin(arad)));
  vmov -> z = (vpiv -> z + (vtmp.x * sin(arad) + vtmp.z * cos(arad)));
}
//---------------------------------------------------------------------------

void Tp_camera::rotaciona_no_eixo_z(Tp_vrt *vpiv, Tp_vrt *vmov, const float ang)
{
  Tp_vrt vtmp;
  float arad = ang * M_PI / 180.0f;
  calcula_distancia_dos_vertices_em_relacao_ao_pivot(vpiv, vmov, &vtmp);

  vmov -> x = (vpiv -> x + (vtmp.x * cos(arad) + vtmp.y * sin(arad)));
  vmov -> y = (vpiv -> y + (-vtmp.x * sin(arad) + vtmp.y * cos(arad)));
}
//---------------------------------------------------------------------------

void Tp_camera::desenha_3d(const float fovy, const float aspect, const float znear, const float zfar)
{
  glMatrixMode(GL_PROJECTION); glLoadIdentity();
  gluPerspective(fovy, aspect, znear, zfar); glMatrixMode(GL_MODELVIEW); glLoadIdentity();

  switch (vetor_up)
  {
    case vu_y : gluLookAt(vobs -> x, vobs -> y, vobs -> z, valv -> x, valv -> y, valv -> z, 0.0f, 1.0f, 0.0f); break;
    case vu_z : gluLookAt(vobs -> x, vobs -> y, vobs -> z, valv -> x, valv -> y, valv -> z, 0.0f, 0.0f, 1.0f); break; default : break;
  }
}
//---------------------------------------------------------------------------

void Tp_camera::calcula_distancia_dos_vertices_em_relacao_ao_pivot(Tp_vrt *vpiv, Tp_vrt *vmov, Tp_vrt *vtmp)
{
  vtmp -> x = vmov -> x - vpiv -> x; vtmp -> y = vmov -> y - vpiv -> y; vtmp -> z = vmov -> z - vpiv -> z;
}
//---------------------------------------------------------------------------

void Tp_camera::desenha_2d(const float l, const float r, const float b, const float t, const float znear, const float zfar)
{
  glMatrixMode(GL_PROJECTION); glLoadIdentity();
  glOrthof(l, r, b, t, znear, zfar);
  glMatrixMode(GL_MODELVIEW); glLoadIdentity();
}
//---------------------------------------------------------------------------

void Tp_camera::set(Tp_vrt *_valv, Tp_vrt *_vobs, const float dis_do_obs_temp_e_o_alv_temp, const float _passo, const float _ang_rot, const Tp_vetor_up vu)
{
  if (valv != NULL){ delete valv; valv = NULL;}
  if (vobs != NULL){ delete vobs; vobs = NULL;}
  if (valv_temp != NULL){ delete valv_temp; valv_temp = NULL;}
  if (vobs_temp != NULL){ delete vobs_temp; vobs_temp = NULL;}

  vetor_up = vu;
  passo = _passo;
  ang_rot = _ang_rot;
  valv_temp = new Tp_vrt(0.0f, 0.0f, 0.0f);
  valv = new Tp_vrt(_valv -> x, _valv -> y, _valv -> z);
  vobs = new Tp_vrt(_vobs -> x, _vobs -> y, _vobs -> z);
  press_w = press_s = press_t = press_g =
  press_up = press_down = press_left = press_right = false;

  switch (vetor_up)
  {
    case vu_y : vobs_temp = new Tp_vrt(0.0f, 0.0f, - dis_do_obs_temp_e_o_alv_temp); break;
    case vu_z : vobs_temp = new Tp_vrt(0.0f, - dis_do_obs_temp_e_o_alv_temp, 0.0f); break; default : break;
  }
}
//---------------------------------------------------------------------------

Tp_camera::Tp_camera(Tp_vrt *_valv, Tp_vrt *_vobs, const float dis_do_obs_temp_e_o_alv_temp, const float _passo, const float _ang_rot, const Tp_vetor_up vu)
{
  vetor_up = vu;
  passo = _passo;
  ang_rot = _ang_rot;
  valv_temp = new Tp_vrt(0.0f, 0.0f, 0.0f);
  valv = new Tp_vrt(_valv -> x, _valv -> y, _valv -> z);
  vobs = new Tp_vrt(_vobs -> x, _vobs -> y, _vobs -> z);
  press_w = press_s = press_t = press_g =
  press_up = press_down = press_left = press_right = false;

  switch (vetor_up)
  {
    case vu_y : vobs_temp = new Tp_vrt(0.0f, 0.0f, - dis_do_obs_temp_e_o_alv_temp); break;
    case vu_z : vobs_temp = new Tp_vrt(0.0f, - dis_do_obs_temp_e_o_alv_temp, 0.0f); break; default : break;
  }
}
//---------------------------------------------------------------------------
// classe Tp_neblina
//---------------------------------------------------------------------------

#pragma mark - classe Tp_neblina
#pragma mark -

Tp_neblina::~Tp_neblina()
{
  glDisable(GL_FOG);
}
//---------------------------------------------------------------------------

Tp_neblina::Tp_neblina(const float cor[], const float inicio, const float fim, const float modo, const float densidade)
{
  glEnable(GL_FOG);
  glFogf(GL_FOG_END, fim); glFogf(GL_FOG_MODE, modo);
  glFogfv(GL_FOG_COLOR, cor); glFogf(GL_FOG_START, inicio);
  glHint(GL_FOG_HINT, GL_NICEST); glFogf(GL_FOG_DENSITY, densidade);
}
//---------------------------------------------------------------------------
// classe Tp_textura
//---------------------------------------------------------------------------

#pragma mark - classe Tp_textura
#pragma mark -

void Tp_textura::set()
{
  glBindTexture(GL_TEXTURE_2D, id_tex);
}
//---------------------------------------------------------------------------

Tp_textura::Tp_textura()
{
  id_tex = 0;
}
//---------------------------------------------------------------------------

Tp_textura::~Tp_textura()
{
  glDeleteTextures(1, &id_tex);
}
//---------------------------------------------------------------------------

void Tp_textura::envia_para_opengl(const bool gera_os_mipmaps, NSString *str_nome_da_textura_no_bundle_sem_ext, NSString *str_ext_sem_ponto, const unsigned char r, const unsigned char g, const unsigned char b)
{
  NSString *arquivo = [[NSBundle mainBundle] pathForResource:str_nome_da_textura_no_bundle_sem_ext ofType:str_ext_sem_ponto];
  NSData *data_arquivo = [NSData dataWithContentsOfFile:arquivo]; UIImage *imagem = [UIImage imageWithData:data_arquivo];

  if (imagem == nil){ NSLog(@"Nao carregou a imagem."); return;}

  unsigned int largura = (unsigned int) CGImageGetWidth(imagem.CGImage);
  unsigned int comprimento = (unsigned int) CGImageGetHeight(imagem.CGImage);
  CGColorSpaceRef color_space = CGColorSpaceCreateDeviceRGB(); void *pixels = malloc(largura * comprimento * 4);
  CGContextRef contexto = CGBitmapContextCreate(pixels, largura, comprimento, 8, 4 * largura, color_space, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
  CGColorSpaceRelease(color_space); CGContextClearRect(contexto, CGRectMake(0.0f, 0.0f, largura, comprimento)); CGContextTranslateCTM(contexto, 0.0f, comprimento - comprimento);
  CGContextDrawImage(contexto, CGRectMake(0.0f, 0.0f, largura, comprimento), imagem.CGImage);

  glGenTextures(1, &id_tex); set();

  glHint(GL_GENERATE_MIPMAP_HINT, GL_NICEST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, gera_os_mipmaps ? GL_TRUE : GL_FALSE);

  unsigned int cont = 0;
  unsigned char *pix = (unsigned char*) pixels;
  unsigned int tam = (largura * comprimento) * 4;

  while (cont < tam)
  {
    pix[cont + 3] = ((pix[cont] != r) || (pix[cont + 1] != g) || (pix[cont + 2] != b)) ? 255 : 0;
    cont += 4;
  }

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, largura, comprimento, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

  CGContextRelease(contexto); free(pixels);
}
//---------------------------------------------------------------------------

