//---------------------------------------------------------------------------

#ifndef U_controladorH
#define U_controladorH
//---------------------------------------------------------------------------

#include "U_damas.h"
//---------------------------------------------------------------------------
// classe Tp_controlador
//---------------------------------------------------------------------------

#pragma mark - classe Tp_controlador
#pragma mark -

class Tp_controlador
{
private :
  Ns_damas::Tp_damas *damas;

  void trabalha();
  void desenha(const float larg, const float comp);
public :
  bool sel;

  Tp_controlador();
  ~Tp_controlador();
  void loop_principal(const float larg, const float comp);
  void touchesBegan(NSSet *touches, UIEvent *event, UIView *view);
  void touchesMoved(NSSet *touches, UIEvent *event, UIView *view);
  void touchesEnded(NSSet *touches, UIEvent *event, UIView *view);
};
//---------------------------------------------------------------------------

extern Tp_controlador *controlador;
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------

