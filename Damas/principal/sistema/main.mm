//---------------------------------------------------------------------------
//
//  Projeto Way 3D 2012
//  Criado em 08/12/2013 as 11h:48m:28s.
//
//---------------------------------------------------------------------------

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
//---------------------------------------------------------------------------
// funcao principal
//---------------------------------------------------------------------------

#pragma mark - funcao principal
#pragma mark -

int main (int argc, char *argv[])
{
  @autoreleasepool
  {
    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
//---------------------------------------------------------------------------

