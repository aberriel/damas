//---------------------------------------------------------------------------

#import "ViewController.h"
//---------------------------------------------------------------------------
// classe ViewController
//---------------------------------------------------------------------------

#pragma mark - classe ViewController
#pragma mark -

@implementation ViewController

@synthesize context = _context;
//---------------------------------------------------------------------------

- (void) dealloc
{
#if !__has_feature(objc_arc)
  [_context release]; [super dealloc];
#endif
}
//---------------------------------------------------------------------------

- (void) inicia_gl
{
  [EAGLContext setCurrentContext:self.context]; controlador = new Tp_controlador();
}
//---------------------------------------------------------------------------

- (void) finaliza_gl
{
  [EAGLContext setCurrentContext:self.context]; delete controlador; controlador = NULL;
}
//---------------------------------------------------------------------------

- (void) viewDidLoad
{
  [super viewDidLoad];

  larg = comp = 0.0f;
  [self setPreferredFramesPerSecond:60];
  [self.view setMultipleTouchEnabled:YES];
#if __has_feature(objc_arc)
  self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1];
#else
  self.context = [[[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1] autorelease];
#endif

  if (! (self.context)) NSLog(@"Falha ao criar contexto ES"); GLKView *view = (GLKView*) self.view;
  view.context = self.context; view.drawableDepthFormat = GLKViewDrawableDepthFormat24; [self inicia_gl];
}
//---------------------------------------------------------------------------

- (void) viewDidUnload
{
  [super viewDidUnload]; [self finaliza_gl];

  if ([EAGLContext currentContext] == self.context) [EAGLContext setCurrentContext:nil]; self.context = nil;
}
//---------------------------------------------------------------------------

- (void) didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
}
//---------------------------------------------------------------------------

- (void) touchesBegan:(NSSet*) touches withEvent:(UIEvent*) event
{
  if (controlador == NULL) return;
  controlador -> touchesBegan(touches, event, self.view);
}
//---------------------------------------------------------------------------

- (void) touchesMoved:(NSSet*) touches withEvent:(UIEvent*) event
{
  if (controlador == NULL) return;
  controlador -> touchesMoved(touches, event, self.view);
}
//---------------------------------------------------------------------------

- (void) touchesEnded:(NSSet*) touches withEvent:(UIEvent*) event
{
  if (controlador == NULL) return;
  controlador -> touchesEnded(touches, event, self.view);
}
//---------------------------------------------------------------------------

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation
{
  return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) ||
          (interfaceOrientation == UIInterfaceOrientationLandscapeRight));
}
//---------------------------------------------------------------------------

#pragma mark - GLKViewDelegate
#pragma mark -

- (void) update
{
  if (controlador == NULL) return;
  if (controlador -> sel) controlador -> loop_principal(larg, comp);
}
//---------------------------------------------------------------------------

- (void) glkView:(GLKView*) view drawInRect:(CGRect) rect
{
  if (controlador == NULL) return;

  int viewport[4];
  glGetIntegerv(GL_VIEWPORT, viewport);
  larg = (float)viewport[2]; comp = (float)viewport[3];

  if (! (controlador -> sel)) controlador -> loop_principal(larg, comp);
}
//---------------------------------------------------------------------------

@end
//---------------------------------------------------------------------------

