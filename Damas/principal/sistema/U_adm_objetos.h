//---------------------------------------------------------------------------

#ifndef U_adm_objetosH
#define U_adm_objetosH
//---------------------------------------------------------------------------

#include "U_glcers.h"
//---------------------------------------------------------------------------
// classe Tp_adm_objetos
//---------------------------------------------------------------------------

#pragma mark - classe Tp_adm_objetos
#pragma mark -

class Tp_adm_objetos
{
protected :
  unsigned int id_coortex;
  unsigned int id_normais;
  unsigned int id_vertice;

  void set_buffers(const bool sel);
public :
  char *nome;
  int xx, yy;
  float x, y, z;
  bool virou_dama;
  float *cor_solida;
  bool foi_selecionado;
  unsigned char *cor_de_selecao;
  Tp_jogador_da_vez jogador_da_vez;
  bool foi_marcado_para_ser_deletado;

  ~Tp_adm_objetos();
  virtual void desenha(const bool sel) = 0;
  void set_indx(const int _xx, const int _yy);
  void set_pos(const float _x, const float _y, const float _z);
  Tp_adm_objetos(const unsigned char r, const unsigned char g, const unsigned char b);
  void set_pos_e_indx(const float _x, const float _y, const float _z, const int _xx, const int _yy);
  void set_pos_e_indx_e_jogador_da_vez(const float _x, const float _y, const float _z, const int _xx, const int _yy, const Tp_jogador_da_vez _jogador_da_vez);
};
//---------------------------------------------------------------------------
// classe Tp_dados_do_tabuleiro
//---------------------------------------------------------------------------

#pragma mark - classe Tp_dados_do_tabuleiro
#pragma mark -

class Tp_dados_do_tabuleiro
{
public :
  int xx, yy;
  float x, y, z;
  bool esta_ocupado;
  bool e_lugar_valido;
  Tp_adm_objetos *adm_objeto;
  bool deleta_a_peca_se_selecionado;

  Tp_dados_do_tabuleiro();
  ~Tp_dados_do_tabuleiro();
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------

