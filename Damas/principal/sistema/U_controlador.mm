//---------------------------------------------------------------------------

#include "U_controlador.h"
//---------------------------------------------------------------------------
// classe Tp_controlador
//---------------------------------------------------------------------------

#pragma mark - classe Tp_controlador
#pragma mark -

Tp_controlador::Tp_controlador()
{
  sel = false;

  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  //glEnable(GL_ALPHA_TEST);
  glEnable(GL_COLOR_MATERIAL);

  glAlphaFunc(GL_EQUAL, 1.0f);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE);
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_NORMAL_ARRAY);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);

  damas = new Ns_damas::Tp_damas();
}
//---------------------------------------------------------------------------

Tp_controlador::~Tp_controlador()
{
  delete damas; damas = NULL;

  glDisableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_NORMAL_ARRAY);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}
//---------------------------------------------------------------------------

void Tp_controlador::trabalha()
{
  damas -> trabalha();

  if (damas -> fim_de_jogo)
  {
    delete damas; damas = NULL; damas = new Ns_damas::Tp_damas();
  }
}
//---------------------------------------------------------------------------

void Tp_controlador::desenha(const float larg, const float comp)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  damas -> desenha(sel, larg, comp); sel = false;
}
//---------------------------------------------------------------------------

void Tp_controlador::loop_principal(const float larg, const float comp)
{
  trabalha(); desenha(larg, comp);
}
//---------------------------------------------------------------------------

void Tp_controlador::touchesBegan(NSSet *touches, UIEvent *event, UIView *view)
{
  sel = true;
  damas -> touchesBegan(touches, event, view);
}
//---------------------------------------------------------------------------

void Tp_controlador::touchesMoved(NSSet *touches, UIEvent *event, UIView *view)
{
  damas -> touchesMoved(touches, event, view);
}
//---------------------------------------------------------------------------

void Tp_controlador::touchesEnded(NSSet *touches, UIEvent *event, UIView *view)
{
  sel = false;
  damas -> touchesEnded(touches, event, view);
}
//---------------------------------------------------------------------------

Tp_controlador *controlador = NULL;
//---------------------------------------------------------------------------

