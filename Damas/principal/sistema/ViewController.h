//---------------------------------------------------------------------------

#import "U_controlador.h"
//---------------------------------------------------------------------------
// classe ViewController
//---------------------------------------------------------------------------

#pragma mark - classe ViewController
#pragma mark -

@interface ViewController : GLKViewController
{
  float larg, comp;
}

@property (strong, nonatomic) EAGLContext *context;

- (void) inicia_gl;
- (void) finaliza_gl;

@end
//---------------------------------------------------------------------------

