//---------------------------------------------------------------------------

#include "U_recursos.h"
//---------------------------------------------------------------------------

int xtouch = -1, ytouch = -1;
//---------------------------------------------------------------------------

#pragma mark - funcoes auxiliares
#pragma mark -

Tp_retina retina_display(const float larg, const float comp)
{
  if (((larg == 640.0f) && (comp == 960.0f)) || ((larg == 960.0f) && (comp == 640.0f))) // iPhone 3.5 polegadas
    return re_3_5;
  if (((larg == 640.0f) && (comp == 1136.0f)) || ((larg == 1136.0f) && (comp == 640.0f))) // iPhone 4.0 polegadas
    return re_4_0;
  if (((larg == 768.0f) && (comp == 1024.0f)) || ((larg == 1024.0f) && (comp == 768.0f))) // iPad ou iPad Mini sem retina
    return re_ipad_sem_retina;
  if (((larg == 1536.0f) && (comp == 2048.0f)) || ((larg == 2048.0f) && (comp == 1536.0f))) // iPad ou iPad Mini com retina
    return re_ipad_com_retina;
  return re_null;
}
//---------------------------------------------------------------------------

void gluPerspective(const double fovy, const double aspect, const double znear, const double zfar)
{
  double xmin, xmax, ymin, ymax;

  ymax = znear * tan(fovy * M_PI / 360.0f);
  ymin = -ymax;
  xmin = ymin * aspect;
  xmax = ymax * aspect;

  glFrustumf(xmin, xmax, ymin, ymax, znear, zfar);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); glDepthMask(GL_TRUE);
}
//---------------------------------------------------------------------------

void gluLookAt(const float eyex, const float eyey, const float eyez, const float centerx, const float centery, const float centerz, const float upx, const float upy, const float upz)
{
  float mag, m[16], x[3], y[3], z[3];

  z[0] = eyex - centerx;
  z[1] = eyey - centery;
  z[2] = eyez - centerz;

  mag = sqrt(z[0] * z[0] + z[1] * z[1] + z[2] * z[2]);
  if (mag){	z[0] /= mag; z[1] /= mag; z[2] /= mag;}

  y[0] = upx; y[1] = upy; y[2] = upz;

  x[0] = y[1] * z[2] - y[2] * z[1];
  x[1] = -y[0] * z[2] + y[2] * z[0];
  x[2] = y[0] * z[1] - y[1] * z[0];

  y[0] = z[1] * x[2] - z[2] * x[1];
  y[1] = -z[0] * x[2] + z[2] * x[0];
  y[2] = z[0] * x[1] - z[1] * x[0];

  mag = sqrt(x[0] * x[0] + x[1] * x[1] + x[2] * x[2]);
  if (mag){	x[0] /= mag; x[1] /= mag; x[2] /= mag;}

  mag = sqrt(y[0] * y[0] + y[1] * y[1] + y[2] * y[2]);
  if (mag){	y[0] /= mag; y[1] /= mag; y[2] /= mag;}

#define M(row, col)  m[col * 4 + row]
  M(0, 0) = x[0]; M(0, 1) = x[1]; M(0, 2) = x[2]; M(0, 3) = 0.0f;
  M(1, 0) = y[0]; M(1, 1) = y[1]; M(1, 2) = y[2]; M(1, 3) = 0.0f;
  M(2, 0) = z[0]; M(2, 1) = z[1]; M(2, 2) = z[2]; M(2, 3) = 0.0f;
  M(3, 0) = 0.0f; M(3, 1) = 0.0f; M(3, 2) = 0.0f; M(3, 3) = 1.0f;
#undef M

  glMultMatrixf(m); glTranslatef(-eyex, -eyey, -eyez);
}
//---------------------------------------------------------------------------

