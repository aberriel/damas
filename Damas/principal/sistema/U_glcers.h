//---------------------------------------------------------------------------

#ifndef U_glcersH
#define U_glcersH
//---------------------------------------------------------------------------

#include "U_recursos.h"
//---------------------------------------------------------------------------
// classe Tp_luz
//---------------------------------------------------------------------------

#pragma mark - classe Tp_luz
#pragma mark -

class Tp_luz
{
private :
  int indx_da_luz;
  float pos_da_luz[4];
  float cor_difusa[4];
  float cor_especular[4];
public :
  ~Tp_luz();
  void desenha();
  void set_pos_da_luz(const float x, const float y, const float z, const float a);
  void set_cor_difusa(const float r, const float g, const float b, const float a);
  void set_cor_especular(const float r, const float g, const float b, const float a);
  Tp_luz(const int _indx_da_luz, const float _pos_da_luz[], const float _cor_difusa[], const float _cor_especular[]);
};
//---------------------------------------------------------------------------
// classe Tp_vrt
//---------------------------------------------------------------------------

#pragma mark - classe Tp_vrt
#pragma mark -

class Tp_vrt
{
public :
  float x, y, z;

  Tp_vrt();
  ~Tp_vrt();
  Tp_vrt(const float _x, const float _y, const float _z);
};
//---------------------------------------------------------------------------
// classe Tp_camera
//---------------------------------------------------------------------------

#pragma mark - classe Tp_camera
#pragma mark -

class Tp_camera
{
protected :
  float passo;
  float ang_rot;
  Tp_vrt *valv_temp;
  Tp_vrt *vobs_temp;
  Tp_vetor_up vetor_up;

  void rotaciona_no_eixo_x(Tp_vrt *vpiv, Tp_vrt *vmov, const float ang);
  void rotaciona_no_eixo_y(Tp_vrt *vpiv, Tp_vrt *vmov, const float ang);
  void rotaciona_no_eixo_z(Tp_vrt *vpiv, Tp_vrt *vmov, const float ang);
  void calcula_distancia_dos_vertices_em_relacao_ao_pivot(Tp_vrt *vpiv, Tp_vrt *vmov, Tp_vrt *vtmp);
public :
  Tp_vrt *valv;
  Tp_vrt *vobs;
  bool press_w;
  bool press_s;
  bool press_t;
  bool press_g;
  bool press_up;
  bool press_down;
  bool press_left;
  bool press_right;

  Tp_camera();
  ~Tp_camera();
  void trabalha();
  void desenha_3d(const float fovy, const float aspect, const float znear, const float zfar);
  void desenha_2d(const float l, const float r, const float b, const float t, const float znear, const float zfar);
  void set(Tp_vrt *_valv, Tp_vrt *_vobs, const float dis_do_obs_temp_e_o_alv_temp, const float _passo, const float _ang_rot, const Tp_vetor_up vu);
  Tp_camera(Tp_vrt *_valv, Tp_vrt *_vobs, const float dis_do_obs_temp_e_o_alv_temp, const float _passo, const float _ang_rot, const Tp_vetor_up vu);
};
//---------------------------------------------------------------------------
// classe Tp_neblina
//---------------------------------------------------------------------------

#pragma mark - classe Tp_neblina
#pragma mark -

class Tp_neblina
{
public :
  ~Tp_neblina();
  Tp_neblina(const float cor[], const float inicio, const float fim, const float modo, const float densidade);
};
//---------------------------------------------------------------------------
// classe Tp_textura
//---------------------------------------------------------------------------

#pragma mark - classe Tp_textura
#pragma mark -

class Tp_textura
{
public :
  unsigned int id_tex;

  void set();
  Tp_textura();
  ~Tp_textura();
  void envia_para_opengl(const bool gera_os_mipmaps, NSString *str_nome_da_textura_no_bundle_sem_ext, NSString *str_ext_sem_ponto, const unsigned char r, const unsigned char g, const unsigned char b);
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------

