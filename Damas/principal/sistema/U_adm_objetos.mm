//---------------------------------------------------------------------------

#include "U_adm_objetos.h"
//---------------------------------------------------------------------------
// classe Tp_adm_objetos
//---------------------------------------------------------------------------

#pragma mark - classe Tp_adm_objetos
#pragma mark -

Tp_adm_objetos::~Tp_adm_objetos()
{
  delete [] nome; nome = NULL;
  delete [] cor_solida; cor_solida = NULL;
  delete [] cor_de_selecao; cor_de_selecao = NULL;

  if (id_coortex != 0) glDeleteBuffers(1, &id_coortex);
  if (id_normais != 0) glDeleteBuffers(1, &id_normais);
  if (id_vertice != 0) glDeleteBuffers(1, &id_vertice);

  id_coortex = id_normais = id_vertice = 0;
}
//---------------------------------------------------------------------------

void Tp_adm_objetos::set_buffers(const bool sel)
{
  if (! (sel))
  {
    if (foi_selecionado)
      glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
    else
      glColor4f(cor_solida[0], cor_solida[1], cor_solida[2], cor_solida[3]);
  }
  else
    glColor4ub(cor_de_selecao[0], cor_de_selecao[1], cor_de_selecao[2], cor_de_selecao[3]);

  glBindBuffer(GL_ARRAY_BUFFER, id_normais == 0 ? -1 : id_normais); glNormalPointer(GL_FLOAT, 0, NULL);
  glBindBuffer(GL_ARRAY_BUFFER, id_vertice == 0 ? -1 : id_vertice); glVertexPointer(3, GL_FLOAT, 0, NULL);
  glBindBuffer(GL_ARRAY_BUFFER, id_coortex == 0 ? -1 : id_coortex); glTexCoordPointer(2, GL_FLOAT, 0, NULL);
}
//---------------------------------------------------------------------------

void Tp_adm_objetos::set_indx(const int _xx, const int _yy)
{
  xx = _xx; yy = _yy;
}
//---------------------------------------------------------------------------

void Tp_adm_objetos::set_pos(const float _x, const float _y, const float _z)
{
  x = _x; y = _y; z = _z;
}
//---------------------------------------------------------------------------

Tp_adm_objetos::Tp_adm_objetos(const unsigned char r, const unsigned char g, const unsigned char b)
{
  nome = NULL;
  x = y = z = 0.0f;
  virou_dama = false;
  foi_selecionado = false;
  jogador_da_vez = jv_null;
  cor_solida = new float[MAX_COR];
  foi_marcado_para_ser_deletado = false;
  id_coortex = id_normais = id_vertice = 0;
  cor_de_selecao = new unsigned char[MAX_COR];
  cor_solida[0] = 1.0f; cor_solida[1] = 1.0f; cor_solida[2] = 1.0f; cor_solida[3] = 1.0f;
  cor_de_selecao[0] = r; cor_de_selecao[1] = g; cor_de_selecao[2] = b; cor_de_selecao[3] = 255;
}
//---------------------------------------------------------------------------

void Tp_adm_objetos::set_pos_e_indx(const float _x, const float _y, const float _z, const int _xx, const int _yy)
{
  xx = _xx; yy = _yy;
  x = _x; y = _y; z = _z;
}
//---------------------------------------------------------------------------

void Tp_adm_objetos::set_pos_e_indx_e_jogador_da_vez(const float _x, const float _y, const float _z, const int _xx, const int _yy, const Tp_jogador_da_vez _jogador_da_vez)
{
  xx = _xx; yy = _yy;
  x = _x; y = _y; z = _z;
  jogador_da_vez = _jogador_da_vez;
}
//---------------------------------------------------------------------------
// classe Tp_dados_do_tabuleiro
//---------------------------------------------------------------------------

#pragma mark - classe Tp_dados_do_tabuleiro
#pragma mark -

Tp_dados_do_tabuleiro::Tp_dados_do_tabuleiro()
{
  adm_objeto = nil;
  x = y = z = xx = yy = 0.0f;
  deleta_a_peca_se_selecionado = false;
  esta_ocupado = e_lugar_valido = false;
}
//---------------------------------------------------------------------------

Tp_dados_do_tabuleiro::~Tp_dados_do_tabuleiro()
{
}
//---------------------------------------------------------------------------

