//---------------------------------------------------------------------------

#import <UIKit/UIKit.h>
//---------------------------------------------------------------------------
// classe ViewController
//---------------------------------------------------------------------------

#pragma mark - classe ViewController
#pragma mark -

@class ViewController;
//---------------------------------------------------------------------------
// classe AppDelegate
//---------------------------------------------------------------------------

#pragma mark - classe AppDelegate
#pragma mark -

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ViewController *viewController;

@end
//---------------------------------------------------------------------------

