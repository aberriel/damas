//---------------------------------------------------------------------------

#ifndef U_recursosH
#define U_recursosH
//---------------------------------------------------------------------------

#import <GLKit/GLKit.h>
//---------------------------------------------------------------------------

#pragma mark - definicoes
#pragma mark -

#ifndef NULL
  #define NULL 0
#endif
//---------------------------------------------------------------------------

#ifndef MAX_COR
  #define MAX_COR 4
#endif
//---------------------------------------------------------------------------

#define TOTAL_DE_PECAS_POR_JOGADOR 12
#define TOTAL_DE_BASES_EM_TODO_O_TABULEIRO 64
#define TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO 8
//---------------------------------------------------------------------------

typedef enum {vu_null = 0, vu_y, vu_z} Tp_vetor_up;
typedef enum {jv_null = 0, jv_branco, jv_madeira} Tp_jogador_da_vez;
typedef enum {re_null = 0, re_3_5, re_4_0, re_ipad_com_retina, re_ipad_sem_retina} Tp_retina;
//---------------------------------------------------------------------------

#pragma mark - funcoes auxiliares
#pragma mark -

Tp_retina retina_display(const float larg, const float comp);
void gluPerspective(const double fovy, const double aspect, const double znear, const double zfar);
void gluLookAt(const float eyex, const float eyey, const float eyez, const float centerx, const float centery, const float centerz, const float upx, const float upy, const float upz);
//---------------------------------------------------------------------------

extern int xtouch, ytouch;
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------

