//---------------------------------------------------------------------------

#include "U_damas.h"
//---------------------------------------------------------------------------
// definicoes
//---------------------------------------------------------------------------

#pragma mark - definicoes
#pragma mark -

#define TEXTURA_DAMAS_00 0
#define PASSO_DO_TABULEIRO 20
#define LIMITE_DO_TABULEIRO 70
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_damas
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_damas
#pragma mark -

Ns_damas::Tp_damas::Tp_damas()
{
  retina = re_null;
  fim_de_jogo = false;
  rot_do_fundo = 0.0f;

  textura = new Tp_textura[1];

  seta_para_baixo_sobe = true;
  rot_da_seta_para_baixo = 0.0f;
  pss_da_seta_para_baixo = 0.0f;

  Tp_jogador_da_vez jv = jv_null;

  fundo_0000000000 = new Tp_fundo_0000000000();
  base_03_0000000000 = new Tp_base_03_0000000000();
  base_04_0000000000 = new Tp_base_04_0000000000();
  base_05_0000000000 = new Tp_base_05_0000000000();
  base_06_0000000000 = new Tp_base_06_0000000000();
  base_07_0000000000 = new Tp_base_07_0000000000();
  base_08_0000000000 = new Tp_base_08_0000000000();
  base_09_0000000000 = new Tp_base_09_0000000000();
  base_10_0000000000 = new Tp_base_10_0000000000();
  base_11_0000000000 = new Tp_base_11_0000000000();
  base_12_0000000000 = new Tp_base_12_0000000000();
  base_13_0000000000 = new Tp_base_13_0000000000();
  base_14_0000000000 = new Tp_base_14_0000000000();
  base_15_0000000000 = new Tp_base_15_0000000000();
  base_16_0000000000 = new Tp_base_16_0000000000();
  base_17_0000000000 = new Tp_base_17_0000000000();
  base_18_0000000000 = new Tp_base_18_0000000000();
  base_19_0000000000 = new Tp_base_19_0000000000();
  base_20_0000000000 = new Tp_base_20_0000000000();
  base_21_0000000000 = new Tp_base_21_0000000000();
  base_22_0000000000 = new Tp_base_22_0000000000();
  base_23_0000000000 = new Tp_base_23_0000000000();
  base_24_0000000000 = new Tp_base_24_0000000000();
  base_25_0000000000 = new Tp_base_25_0000000000();
  base_26_0000000000 = new Tp_base_26_0000000000();
  base_27_0000000000 = new Tp_base_27_0000000000();
  base_28_0000000000 = new Tp_base_28_0000000000();
  base_29_0000000000 = new Tp_base_29_0000000000();
  base_30_0000000000 = new Tp_base_30_0000000000();
  base_31_0000000000 = new Tp_base_31_0000000000();
  base_32_0000000000 = new Tp_base_32_0000000000();
  base_33_0000000000 = new Tp_base_33_0000000000();
  base_34_0000000000 = new Tp_base_34_0000000000();
  base_35_0000000000 = new Tp_base_35_0000000000();
  base_36_0000000000 = new Tp_base_36_0000000000();
  base_37_0000000000 = new Tp_base_37_0000000000();
  base_38_0000000000 = new Tp_base_38_0000000000();
  base_39_0000000000 = new Tp_base_39_0000000000();
  base_40_0000000000 = new Tp_base_40_0000000000();
  base_41_0000000000 = new Tp_base_41_0000000000();
  base_42_0000000000 = new Tp_base_42_0000000000();
  base_43_0000000000 = new Tp_base_43_0000000000();
  base_44_0000000000 = new Tp_base_44_0000000000();
  base_45_0000000000 = new Tp_base_45_0000000000();
  base_46_0000000000 = new Tp_base_46_0000000000();
  base_47_0000000000 = new Tp_base_47_0000000000();
  base_48_0000000000 = new Tp_base_48_0000000000();
  base_49_0000000000 = new Tp_base_49_0000000000();
  base_50_0000000000 = new Tp_base_50_0000000000();
  base_51_0000000000 = new Tp_base_51_0000000000();
  base_52_0000000000 = new Tp_base_52_0000000000();
  base_53_0000000000 = new Tp_base_53_0000000000();
  base_54_0000000000 = new Tp_base_54_0000000000();
  base_55_0000000000 = new Tp_base_55_0000000000();
  base_56_0000000000 = new Tp_base_56_0000000000();
  base_57_0000000000 = new Tp_base_57_0000000000();
  base_58_0000000000 = new Tp_base_58_0000000000();
  base_59_0000000000 = new Tp_base_59_0000000000();
  base_60_0000000000 = new Tp_base_60_0000000000();
  base_61_0000000000 = new Tp_base_61_0000000000();
  base_62_0000000000 = new Tp_base_62_0000000000();
  base_63_0000000000 = new Tp_base_63_0000000000();
  base_64_0000000000 = new Tp_base_64_0000000000();
  base_02_0000000000 = new Tp_base_02_0000000000();
  base_01_0000000000 = new Tp_base_01_0000000000();
  seta_para_baixo_0000000000 = new Tp_seta_para_baixo_0000000000();
  peca_jogador_01_01_0000000000 = new Tp_peca_jogador_01_01_0000000000();
  peca_jogador_01_02_0000000000 = new Tp_peca_jogador_01_02_0000000000();
  peca_jogador_01_03_0000000000 = new Tp_peca_jogador_01_03_0000000000();
  peca_jogador_01_04_0000000000 = new Tp_peca_jogador_01_04_0000000000();
  peca_jogador_01_05_0000000000 = new Tp_peca_jogador_01_05_0000000000();
  peca_jogador_01_06_0000000000 = new Tp_peca_jogador_01_06_0000000000();
  peca_jogador_01_07_0000000000 = new Tp_peca_jogador_01_07_0000000000();
  peca_jogador_01_08_0000000000 = new Tp_peca_jogador_01_08_0000000000();
  peca_jogador_01_09_0000000000 = new Tp_peca_jogador_01_09_0000000000();
  peca_jogador_01_10_0000000000 = new Tp_peca_jogador_01_10_0000000000();
  peca_jogador_01_11_0000000000 = new Tp_peca_jogador_01_11_0000000000();
  peca_jogador_01_12_0000000000 = new Tp_peca_jogador_01_12_0000000000();
  peca_jogador_02_01_0000000000 = new Tp_peca_jogador_02_01_0000000000();
  peca_jogador_02_02_0000000000 = new Tp_peca_jogador_02_02_0000000000();
  peca_jogador_02_03_0000000000 = new Tp_peca_jogador_02_03_0000000000();
  peca_jogador_02_04_0000000000 = new Tp_peca_jogador_02_04_0000000000();
  peca_jogador_02_05_0000000000 = new Tp_peca_jogador_02_05_0000000000();
  peca_jogador_02_06_0000000000 = new Tp_peca_jogador_02_06_0000000000();
  peca_jogador_02_07_0000000000 = new Tp_peca_jogador_02_07_0000000000();
  peca_jogador_02_08_0000000000 = new Tp_peca_jogador_02_08_0000000000();
  peca_jogador_02_09_0000000000 = new Tp_peca_jogador_02_09_0000000000();
  peca_jogador_02_10_0000000000 = new Tp_peca_jogador_02_10_0000000000();
  peca_jogador_02_11_0000000000 = new Tp_peca_jogador_02_11_0000000000();
  peca_jogador_02_12_0000000000 = new Tp_peca_jogador_02_12_0000000000();

  jogador_da_vez = arc4random() % 2 == 0 ? jv_branco : jv_madeira;
  total_de_pecas_branca = total_de_pecas_de_madeira = TOTAL_DE_PECAS_POR_JOGADOR;
  textura[TEXTURA_DAMAS_00].envia_para_opengl(true, @"texturas", @"png", 255, 0, 255);

  int xx = 0, yy = 0;
  for (int y = LIMITE_DO_TABULEIRO; y >= -LIMITE_DO_TABULEIRO; y-=PASSO_DO_TABULEIRO)
    for (int x = -LIMITE_DO_TABULEIRO; x <= LIMITE_DO_TABULEIRO; x+=PASSO_DO_TABULEIRO)
    {
      tabuleiro[xx][yy].x = x;
      tabuleiro[xx][yy].y = y;
      tabuleiro[xx][yy].z = 0.0f;
      tabuleiro[xx][yy].xx = xx; tabuleiro[xx][yy].yy = yy;

      if (! (yy % 2 == 0))
      {
        if (xx % 2 == 0) tabuleiro[xx][yy].e_lugar_valido = true;
      }
      else
      {
        if (! (xx % 2 == 0)) tabuleiro[xx][yy].e_lugar_valido = true;
      }

      xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
    }

  float esp_00[] = {0.3f, 0.3f, 0.3f, 1.0f};
  float dif_00[] = {1.0f, 1.0f, 1.0f, 1.0f};
  float pos_00[] = {200.0f, 200.0f, 300.0f, 1.0f};
  float pos_01[] = {-200.0f, -200.0f, -300.0f, 1.0f};

  luz_00 = new Tp_luz(0, pos_00, dif_00, esp_00); luz_01 = new Tp_luz(1, pos_01, dif_00, esp_00);
  Tp_vrt valv(0.0f, 0.0f, 0.0f); Tp_vrt vobs(215.0f, -215.0f, 178.0f); camera = new Tp_camera(&valv, &vobs, 1.0f, 1.0f, 0.25f, vu_z);

  jv = jv_branco; xx = 1; yy = 0;
  peca_jogador_01_01_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx += 2;
  peca_jogador_01_02_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx += 2;
  peca_jogador_01_03_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx += 2;
  peca_jogador_01_04_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx = 0; yy++;
  peca_jogador_01_05_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx += 2;
  peca_jogador_01_06_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx += 2;
  peca_jogador_01_07_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx += 2;
  peca_jogador_01_08_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx = 1; yy++;
  peca_jogador_01_09_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx += 2;
  peca_jogador_01_10_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx += 2;
  peca_jogador_01_11_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx += 2;
  peca_jogador_01_12_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true;

  jv = jv_madeira; xx = TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO - 2; yy = TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO - 1;
  peca_jogador_02_01_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx -= 2;
  peca_jogador_02_02_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx -= 2;
  peca_jogador_02_03_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx -= 2;
  peca_jogador_02_04_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx = TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO - 1; yy--;
  peca_jogador_02_05_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx -= 2;
  peca_jogador_02_06_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx -= 2;
  peca_jogador_02_07_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx -= 2;
  peca_jogador_02_08_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx = TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO - 2; yy--;
  peca_jogador_02_09_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx -= 2;
  peca_jogador_02_10_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx -= 2;
  peca_jogador_02_11_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true; xx -= 2;
  peca_jogador_02_12_0000000000 -> set_pos_e_indx_e_jogador_da_vez(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy, jv); tabuleiro[xx][yy].esta_ocupado = true;

  int indx = 0; xx = 0; yy = 0;
  base_01_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_02_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_03_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_04_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_05_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_06_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_07_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_08_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_09_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_10_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_11_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_12_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_13_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_14_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_15_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_16_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_17_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_18_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_19_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_20_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_21_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_22_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_23_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_24_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_25_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_26_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_27_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_28_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_29_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_30_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_31_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_32_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_33_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_34_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_35_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_36_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_37_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_38_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_39_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_40_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_41_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_42_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_43_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_44_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_45_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_46_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_47_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_48_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_49_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_50_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_51_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_52_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_53_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_54_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_55_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_56_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_57_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_58_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_59_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_60_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_61_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_62_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_63_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy); tabuleiro[xx][yy].adm_objeto = peca_do_tabuleiro_indice(indx++); xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
  base_64_0000000000 -> set_pos_e_indx(tabuleiro[xx][yy].x, tabuleiro[xx][yy].y, tabuleiro[xx][yy].z, tabuleiro[xx][yy].xx, tabuleiro[xx][yy].yy);
}
//---------------------------------------------------------------------------

Ns_damas::Tp_damas::~Tp_damas()
{
  delete camera; camera = NULL;
  delete luz_00; luz_00 = NULL;
  delete luz_01; luz_01 = NULL;
  delete [] textura; textura = NULL;
  delete fundo_0000000000; fundo_0000000000 = NULL;
  delete base_03_0000000000; base_03_0000000000 = NULL;
  delete base_04_0000000000; base_04_0000000000 = NULL;
  delete base_05_0000000000; base_05_0000000000 = NULL;
  delete base_06_0000000000; base_06_0000000000 = NULL;
  delete base_07_0000000000; base_07_0000000000 = NULL;
  delete base_08_0000000000; base_08_0000000000 = NULL;
  delete base_09_0000000000; base_09_0000000000 = NULL;
  delete base_10_0000000000; base_10_0000000000 = NULL;
  delete base_11_0000000000; base_11_0000000000 = NULL;
  delete base_12_0000000000; base_12_0000000000 = NULL;
  delete base_13_0000000000; base_13_0000000000 = NULL;
  delete base_14_0000000000; base_14_0000000000 = NULL;
  delete base_15_0000000000; base_15_0000000000 = NULL;
  delete base_16_0000000000; base_16_0000000000 = NULL;
  delete base_17_0000000000; base_17_0000000000 = NULL;
  delete base_18_0000000000; base_18_0000000000 = NULL;
  delete base_19_0000000000; base_19_0000000000 = NULL;
  delete base_20_0000000000; base_20_0000000000 = NULL;
  delete base_21_0000000000; base_21_0000000000 = NULL;
  delete base_22_0000000000; base_22_0000000000 = NULL;
  delete base_23_0000000000; base_23_0000000000 = NULL;
  delete base_24_0000000000; base_24_0000000000 = NULL;
  delete base_25_0000000000; base_25_0000000000 = NULL;
  delete base_26_0000000000; base_26_0000000000 = NULL;
  delete base_27_0000000000; base_27_0000000000 = NULL;
  delete base_28_0000000000; base_28_0000000000 = NULL;
  delete base_29_0000000000; base_29_0000000000 = NULL;
  delete base_30_0000000000; base_30_0000000000 = NULL;
  delete base_31_0000000000; base_31_0000000000 = NULL;
  delete base_32_0000000000; base_32_0000000000 = NULL;
  delete base_33_0000000000; base_33_0000000000 = NULL;
  delete base_34_0000000000; base_34_0000000000 = NULL;
  delete base_35_0000000000; base_35_0000000000 = NULL;
  delete base_36_0000000000; base_36_0000000000 = NULL;
  delete base_37_0000000000; base_37_0000000000 = NULL;
  delete base_38_0000000000; base_38_0000000000 = NULL;
  delete base_39_0000000000; base_39_0000000000 = NULL;
  delete base_40_0000000000; base_40_0000000000 = NULL;
  delete base_41_0000000000; base_41_0000000000 = NULL;
  delete base_42_0000000000; base_42_0000000000 = NULL;
  delete base_43_0000000000; base_43_0000000000 = NULL;
  delete base_44_0000000000; base_44_0000000000 = NULL;
  delete base_45_0000000000; base_45_0000000000 = NULL;
  delete base_46_0000000000; base_46_0000000000 = NULL;
  delete base_47_0000000000; base_47_0000000000 = NULL;
  delete base_48_0000000000; base_48_0000000000 = NULL;
  delete base_49_0000000000; base_49_0000000000 = NULL;
  delete base_50_0000000000; base_50_0000000000 = NULL;
  delete base_51_0000000000; base_51_0000000000 = NULL;
  delete base_52_0000000000; base_52_0000000000 = NULL;
  delete base_53_0000000000; base_53_0000000000 = NULL;
  delete base_54_0000000000; base_54_0000000000 = NULL;
  delete base_55_0000000000; base_55_0000000000 = NULL;
  delete base_56_0000000000; base_56_0000000000 = NULL;
  delete base_57_0000000000; base_57_0000000000 = NULL;
  delete base_58_0000000000; base_58_0000000000 = NULL;
  delete base_59_0000000000; base_59_0000000000 = NULL;
  delete base_60_0000000000; base_60_0000000000 = NULL;
  delete base_61_0000000000; base_61_0000000000 = NULL;
  delete base_62_0000000000; base_62_0000000000 = NULL;
  delete base_63_0000000000; base_63_0000000000 = NULL;
  delete base_64_0000000000; base_64_0000000000 = NULL;
  delete base_02_0000000000; base_02_0000000000 = NULL;
  delete base_01_0000000000; base_01_0000000000 = NULL;
  delete seta_para_baixo_0000000000; seta_para_baixo_0000000000 = nil;
  delete peca_jogador_01_01_0000000000; peca_jogador_01_01_0000000000 = NULL;
  delete peca_jogador_01_02_0000000000; peca_jogador_01_02_0000000000 = NULL;
  delete peca_jogador_01_03_0000000000; peca_jogador_01_03_0000000000 = NULL;
  delete peca_jogador_01_04_0000000000; peca_jogador_01_04_0000000000 = NULL;
  delete peca_jogador_01_05_0000000000; peca_jogador_01_05_0000000000 = NULL;
  delete peca_jogador_01_06_0000000000; peca_jogador_01_06_0000000000 = NULL;
  delete peca_jogador_01_07_0000000000; peca_jogador_01_07_0000000000 = NULL;
  delete peca_jogador_01_08_0000000000; peca_jogador_01_08_0000000000 = NULL;
  delete peca_jogador_01_09_0000000000; peca_jogador_01_09_0000000000 = NULL;
  delete peca_jogador_01_10_0000000000; peca_jogador_01_10_0000000000 = NULL;
  delete peca_jogador_01_11_0000000000; peca_jogador_01_11_0000000000 = NULL;
  delete peca_jogador_01_12_0000000000; peca_jogador_01_12_0000000000 = NULL;
  delete peca_jogador_02_01_0000000000; peca_jogador_02_01_0000000000 = NULL;
  delete peca_jogador_02_02_0000000000; peca_jogador_02_02_0000000000 = NULL;
  delete peca_jogador_02_03_0000000000; peca_jogador_02_03_0000000000 = NULL;
  delete peca_jogador_02_04_0000000000; peca_jogador_02_04_0000000000 = NULL;
  delete peca_jogador_02_05_0000000000; peca_jogador_02_05_0000000000 = NULL;
  delete peca_jogador_02_06_0000000000; peca_jogador_02_06_0000000000 = NULL;
  delete peca_jogador_02_07_0000000000; peca_jogador_02_07_0000000000 = NULL;
  delete peca_jogador_02_08_0000000000; peca_jogador_02_08_0000000000 = NULL;
  delete peca_jogador_02_09_0000000000; peca_jogador_02_09_0000000000 = NULL;
  delete peca_jogador_02_10_0000000000; peca_jogador_02_10_0000000000 = NULL;
  delete peca_jogador_02_11_0000000000; peca_jogador_02_11_0000000000 = NULL;
  delete peca_jogador_02_12_0000000000; peca_jogador_02_12_0000000000 = NULL;
}
//---------------------------------------------------------------------------

void Ns_damas::Tp_damas::trabalha()
{
  rot_do_fundo += 0.25f;
  rot_da_seta_para_baixo += 5.0f;
  if (! (seta_para_baixo_sobe))
  {
    pss_da_seta_para_baixo -= 2.0f;
    seta_para_baixo_sobe = pss_da_seta_para_baixo < 0.0f;
  }
  else
  {
    pss_da_seta_para_baixo += 0.75f;
    seta_para_baixo_sobe = pss_da_seta_para_baixo < 20.0f;
  }
}
//---------------------------------------------------------------------------

void Ns_damas::Tp_damas::seleciona_objeto()
{
  int viewport[4];
  unsigned char pixels[MAX_COR];

  glGetIntegerv(GL_VIEWPORT, viewport);
  glReadPixels(xtouch, viewport[3] - ytouch, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

  if (existe_uma_peca_selecionada())
  {
    if ((base_02_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_02_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_02_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_02_0000000000); return;}
    if ((base_03_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_03_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_03_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_03_0000000000); return;}
    if ((base_04_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_04_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_04_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_04_0000000000); return;}
    if ((base_05_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_05_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_05_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_05_0000000000); return;}
    if ((base_06_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_06_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_06_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_06_0000000000); return;}
    if ((base_07_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_07_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_07_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_07_0000000000); return;}
    if ((base_08_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_08_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_08_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_08_0000000000); return;}
    if ((base_01_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_01_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_01_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_01_0000000000); return;}
    if ((base_09_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_09_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_09_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_09_0000000000); return;}
    if ((base_10_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_10_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_10_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_10_0000000000); return;}
    if ((base_11_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_11_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_11_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_11_0000000000); return;}
    if ((base_12_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_12_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_12_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_12_0000000000); return;}
    if ((base_13_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_13_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_13_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_13_0000000000); return;}
    if ((base_14_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_14_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_14_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_14_0000000000); return;}
    if ((base_15_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_15_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_15_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_15_0000000000); return;}
    if ((base_16_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_16_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_16_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_16_0000000000); return;}
    if ((base_17_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_17_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_17_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_17_0000000000); return;}
    if ((base_18_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_18_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_18_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_18_0000000000); return;}
    if ((base_19_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_19_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_19_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_19_0000000000); return;}
    if ((base_20_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_20_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_20_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_20_0000000000); return;}
    if ((base_21_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_21_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_21_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_21_0000000000); return;}
    if ((base_22_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_22_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_22_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_22_0000000000); return;}
    if ((base_23_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_23_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_23_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_23_0000000000); return;}
    if ((base_24_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_24_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_24_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_24_0000000000); return;}
    if ((base_25_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_25_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_25_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_25_0000000000); return;}
    if ((base_26_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_26_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_26_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_26_0000000000); return;}
    if ((base_27_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_27_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_27_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_27_0000000000); return;}
    if ((base_28_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_28_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_28_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_28_0000000000); return;}
    if ((base_29_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_29_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_29_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_29_0000000000); return;}
    if ((base_30_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_30_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_30_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_30_0000000000); return;}
    if ((base_31_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_31_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_31_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_31_0000000000); return;}
    if ((base_32_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_32_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_32_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_32_0000000000); return;}
    if ((base_33_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_33_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_33_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_33_0000000000); return;}
    if ((base_34_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_34_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_34_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_34_0000000000); return;}
    if ((base_35_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_35_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_35_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_35_0000000000); return;}
    if ((base_36_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_36_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_36_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_36_0000000000); return;}
    if ((base_37_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_37_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_37_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_37_0000000000); return;}
    if ((base_38_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_38_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_38_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_38_0000000000); return;}
    if ((base_39_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_39_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_39_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_39_0000000000); return;}
    if ((base_40_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_40_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_40_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_40_0000000000); return;}
    if ((base_41_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_41_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_41_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_41_0000000000); return;}
    if ((base_42_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_42_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_42_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_42_0000000000); return;}
    if ((base_43_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_43_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_43_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_43_0000000000); return;}
    if ((base_44_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_44_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_44_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_44_0000000000); return;}
    if ((base_45_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_45_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_45_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_45_0000000000); return;}
    if ((base_46_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_46_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_46_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_46_0000000000); return;}
    if ((base_47_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_47_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_47_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_47_0000000000); return;}
    if ((base_48_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_48_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_48_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_48_0000000000); return;}
    if ((base_49_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_49_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_49_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_49_0000000000); return;}
    if ((base_50_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_50_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_50_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_50_0000000000); return;}
    if ((base_51_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_51_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_51_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_51_0000000000); return;}
    if ((base_52_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_52_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_52_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_52_0000000000); return;}
    if ((base_53_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_53_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_53_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_53_0000000000); return;}
    if ((base_54_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_54_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_54_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_54_0000000000); return;}
    if ((base_55_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_55_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_55_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_55_0000000000); return;}
    if ((base_56_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_56_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_56_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_56_0000000000); return;}
    if ((base_57_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_57_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_57_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_57_0000000000); return;}
    if ((base_58_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_58_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_58_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_58_0000000000); return;}
    if ((base_59_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_59_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_59_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_59_0000000000); return;}
    if ((base_60_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_60_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_60_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_60_0000000000); return;}
    if ((base_61_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_61_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_61_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_61_0000000000); return;}
    if ((base_62_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_62_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_62_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_62_0000000000); return;}
    if ((base_63_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_63_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_63_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_63_0000000000); return;}
    if ((base_64_0000000000 -> cor_de_selecao[0] == pixels[0]) && (base_64_0000000000 -> cor_de_selecao[1] == pixels[1]) && (base_64_0000000000 -> cor_de_selecao[2] == pixels[2])){ move_a_peca_para_o_local_selecionado(base_64_0000000000); return;}
  }

  switch (jogador_da_vez)
  {
    case jv_branco :
      for (int i = 0; i < TOTAL_DE_PECAS_POR_JOGADOR; i++)
        if (peca_branca_do_indice(i))
          peca_branca_do_indice(i) -> foi_selecionado = false;

      for (int i = 0; i < TOTAL_DE_BASES_EM_TODO_O_TABULEIRO; i++)
        peca_do_tabuleiro_indice(i) -> foi_selecionado = false;

      if ((peca_jogador_02_01_0000000000) && (peca_jogador_02_01_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_02_01_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_02_01_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_02_01_0000000000);
      if ((peca_jogador_02_02_0000000000) && (peca_jogador_02_02_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_02_02_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_02_02_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_02_02_0000000000);
      if ((peca_jogador_02_03_0000000000) && (peca_jogador_02_03_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_02_03_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_02_03_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_02_03_0000000000);
      if ((peca_jogador_02_04_0000000000) && (peca_jogador_02_04_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_02_04_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_02_04_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_02_04_0000000000);
      if ((peca_jogador_02_05_0000000000) && (peca_jogador_02_05_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_02_05_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_02_05_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_02_05_0000000000);
      if ((peca_jogador_02_06_0000000000) && (peca_jogador_02_06_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_02_06_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_02_06_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_02_06_0000000000);
      if ((peca_jogador_02_07_0000000000) && (peca_jogador_02_07_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_02_07_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_02_07_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_02_07_0000000000);
      if ((peca_jogador_02_08_0000000000) && (peca_jogador_02_08_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_02_08_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_02_08_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_02_08_0000000000);
      if ((peca_jogador_02_09_0000000000) && (peca_jogador_02_09_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_02_09_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_02_09_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_02_09_0000000000);
      if ((peca_jogador_02_10_0000000000) && (peca_jogador_02_10_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_02_10_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_02_10_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_02_10_0000000000);
      if ((peca_jogador_02_11_0000000000) && (peca_jogador_02_11_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_02_11_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_02_11_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_02_11_0000000000);
      if ((peca_jogador_02_12_0000000000) && (peca_jogador_02_12_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_02_12_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_02_12_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_02_12_0000000000);
    break;
    case jv_madeira :
      for (int i = 0; i < TOTAL_DE_PECAS_POR_JOGADOR; i++)
        if (peca_de_madeira_do_indice(i))
          peca_de_madeira_do_indice(i) -> foi_selecionado = false;

      for (int i = 0; i < TOTAL_DE_BASES_EM_TODO_O_TABULEIRO; i++)
        peca_do_tabuleiro_indice(i) -> foi_selecionado = false;

      if ((peca_jogador_01_01_0000000000) && (peca_jogador_01_01_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_01_01_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_01_01_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_01_01_0000000000);
      if ((peca_jogador_01_02_0000000000) && (peca_jogador_01_02_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_01_02_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_01_02_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_01_02_0000000000);
      if ((peca_jogador_01_03_0000000000) && (peca_jogador_01_03_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_01_03_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_01_03_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_01_03_0000000000);
      if ((peca_jogador_01_04_0000000000) && (peca_jogador_01_04_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_01_04_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_01_04_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_01_04_0000000000);
      if ((peca_jogador_01_05_0000000000) && (peca_jogador_01_05_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_01_05_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_01_05_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_01_05_0000000000);
      if ((peca_jogador_01_06_0000000000) && (peca_jogador_01_06_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_01_06_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_01_06_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_01_06_0000000000);
      if ((peca_jogador_01_07_0000000000) && (peca_jogador_01_07_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_01_07_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_01_07_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_01_07_0000000000);
      if ((peca_jogador_01_08_0000000000) && (peca_jogador_01_08_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_01_08_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_01_08_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_01_08_0000000000);
      if ((peca_jogador_01_09_0000000000) && (peca_jogador_01_09_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_01_09_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_01_09_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_01_09_0000000000);
      if ((peca_jogador_01_10_0000000000) && (peca_jogador_01_10_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_01_10_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_01_10_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_01_10_0000000000);
      if ((peca_jogador_01_11_0000000000) && (peca_jogador_01_11_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_01_11_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_01_11_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_01_11_0000000000);
      if ((peca_jogador_01_12_0000000000) && (peca_jogador_01_12_0000000000 -> cor_de_selecao[0] == pixels[0]) && (peca_jogador_01_12_0000000000 -> cor_de_selecao[1] == pixels[1]) && (peca_jogador_01_12_0000000000 -> cor_de_selecao[2] == pixels[2])) selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(peca_jogador_01_12_0000000000);
    break;
    default : break;
  }
}
//---------------------------------------------------------------------------

bool Ns_damas::Tp_damas::existe_uma_peca_selecionada()
{
  switch (jogador_da_vez)
  {
    case jv_branco :
      for (int i = 0; i < TOTAL_DE_PECAS_POR_JOGADOR; i++)
        if (peca_branca_do_indice(i))
          if (peca_branca_do_indice(i) -> foi_selecionado) return true;
    break;
    case jv_madeira :
      for (int i = 0; i < TOTAL_DE_PECAS_POR_JOGADOR; i++)
        if (peca_de_madeira_do_indice(i))
          if (peca_de_madeira_do_indice(i) -> foi_selecionado) return true;
    break;
    default : break;
  }

  return false;
}
//---------------------------------------------------------------------------

void Ns_damas::Tp_damas::deleta_a_peca(Tp_adm_objetos *adm)
{
  int xx = 0, yy = 0;
  bool encontrou = false;

  for (int y = LIMITE_DO_TABULEIRO; ((! (encontrou)) && (y >= -LIMITE_DO_TABULEIRO)); y-=PASSO_DO_TABULEIRO)
    for (int x = -LIMITE_DO_TABULEIRO; ((! (encontrou)) && (x <= LIMITE_DO_TABULEIRO)); x+=PASSO_DO_TABULEIRO)
    {
      if ((adm -> xx == tabuleiro[xx][yy].xx) && (adm -> yy == tabuleiro[xx][yy].yy))
      {
        encontrou = true; tabuleiro[xx][yy].esta_ocupado = false;
      }

      xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
    }

  if (adm == peca_jogador_01_01_0000000000){ delete peca_jogador_01_01_0000000000; peca_jogador_01_01_0000000000 = NULL; return;}
  if (adm == peca_jogador_01_02_0000000000){ delete peca_jogador_01_02_0000000000; peca_jogador_01_02_0000000000 = NULL; return;}
  if (adm == peca_jogador_01_03_0000000000){ delete peca_jogador_01_03_0000000000; peca_jogador_01_03_0000000000 = NULL; return;}
  if (adm == peca_jogador_01_04_0000000000){ delete peca_jogador_01_04_0000000000; peca_jogador_01_04_0000000000 = NULL; return;}
  if (adm == peca_jogador_01_05_0000000000){ delete peca_jogador_01_05_0000000000; peca_jogador_01_05_0000000000 = NULL; return;}
  if (adm == peca_jogador_01_06_0000000000){ delete peca_jogador_01_06_0000000000; peca_jogador_01_06_0000000000 = NULL; return;}
  if (adm == peca_jogador_01_07_0000000000){ delete peca_jogador_01_07_0000000000; peca_jogador_01_07_0000000000 = NULL; return;}
  if (adm == peca_jogador_01_08_0000000000){ delete peca_jogador_01_08_0000000000; peca_jogador_01_08_0000000000 = NULL; return;}
  if (adm == peca_jogador_01_09_0000000000){ delete peca_jogador_01_09_0000000000; peca_jogador_01_09_0000000000 = NULL; return;}
  if (adm == peca_jogador_01_10_0000000000){ delete peca_jogador_01_10_0000000000; peca_jogador_01_10_0000000000 = NULL; return;}
  if (adm == peca_jogador_01_11_0000000000){ delete peca_jogador_01_11_0000000000; peca_jogador_01_11_0000000000 = NULL; return;}
  if (adm == peca_jogador_01_12_0000000000){ delete peca_jogador_01_12_0000000000; peca_jogador_01_12_0000000000 = NULL; return;}
  if (adm == peca_jogador_02_01_0000000000){ delete peca_jogador_02_01_0000000000; peca_jogador_02_01_0000000000 = NULL; return;}
  if (adm == peca_jogador_02_02_0000000000){ delete peca_jogador_02_02_0000000000; peca_jogador_02_02_0000000000 = NULL; return;}
  if (adm == peca_jogador_02_03_0000000000){ delete peca_jogador_02_03_0000000000; peca_jogador_02_03_0000000000 = NULL; return;}
  if (adm == peca_jogador_02_04_0000000000){ delete peca_jogador_02_04_0000000000; peca_jogador_02_04_0000000000 = NULL; return;}
  if (adm == peca_jogador_02_05_0000000000){ delete peca_jogador_02_05_0000000000; peca_jogador_02_05_0000000000 = NULL; return;}
  if (adm == peca_jogador_02_06_0000000000){ delete peca_jogador_02_06_0000000000; peca_jogador_02_06_0000000000 = NULL; return;}
  if (adm == peca_jogador_02_07_0000000000){ delete peca_jogador_02_07_0000000000; peca_jogador_02_07_0000000000 = NULL; return;}
  if (adm == peca_jogador_02_08_0000000000){ delete peca_jogador_02_08_0000000000; peca_jogador_02_08_0000000000 = NULL; return;}
  if (adm == peca_jogador_02_09_0000000000){ delete peca_jogador_02_09_0000000000; peca_jogador_02_09_0000000000 = NULL; return;}
  if (adm == peca_jogador_02_10_0000000000){ delete peca_jogador_02_10_0000000000; peca_jogador_02_10_0000000000 = NULL; return;}
  if (adm == peca_jogador_02_11_0000000000){ delete peca_jogador_02_11_0000000000; peca_jogador_02_11_0000000000 = NULL; return;}
  if (adm == peca_jogador_02_12_0000000000){ delete peca_jogador_02_12_0000000000; peca_jogador_02_12_0000000000 = NULL; return;}
}
//---------------------------------------------------------------------------

Tp_adm_objetos *Ns_damas::Tp_damas::peca_branca_do_indice(const int indx)
{
  switch (indx)
  {
    case 0 : return peca_jogador_02_01_0000000000;
    case 1 : return peca_jogador_02_02_0000000000;
    case 2 : return peca_jogador_02_03_0000000000;
    case 3 : return peca_jogador_02_04_0000000000;
    case 4 : return peca_jogador_02_05_0000000000;
    case 5 : return peca_jogador_02_06_0000000000;
    case 6 : return peca_jogador_02_07_0000000000;
    case 7 : return peca_jogador_02_08_0000000000;
    case 8 : return peca_jogador_02_09_0000000000;
    case 9 : return peca_jogador_02_10_0000000000;
    case 10 : return peca_jogador_02_11_0000000000;
    case 11 : return peca_jogador_02_12_0000000000;
    default : return NULL;
  }
}
//---------------------------------------------------------------------------

Tp_adm_objetos *Ns_damas::Tp_damas::peca_do_tabuleiro_indice(const int indx)
{
  switch (indx)
  {
    case 0 : return base_01_0000000000;
    case 1 : return base_02_0000000000;
    case 2 : return base_03_0000000000;
    case 3 : return base_04_0000000000;
    case 4 : return base_05_0000000000;
    case 5 : return base_06_0000000000;
    case 6 : return base_07_0000000000;
    case 7 : return base_08_0000000000;
    case 8 : return base_09_0000000000;
    case 9 : return base_10_0000000000;
    case 10 : return base_11_0000000000;
    case 11 : return base_12_0000000000;
    case 12 : return base_13_0000000000;
    case 13 : return base_14_0000000000;
    case 14 : return base_15_0000000000;
    case 15 : return base_16_0000000000;
    case 16 : return base_17_0000000000;
    case 17 : return base_18_0000000000;
    case 18 : return base_19_0000000000;
    case 19 : return base_20_0000000000;
    case 20 : return base_21_0000000000;
    case 21 : return base_22_0000000000;
    case 22 : return base_23_0000000000;
    case 23 : return base_24_0000000000;
    case 24 : return base_25_0000000000;
    case 25 : return base_26_0000000000;
    case 26 : return base_27_0000000000;
    case 27 : return base_28_0000000000;
    case 28 : return base_29_0000000000;
    case 29 : return base_30_0000000000;
    case 30 : return base_31_0000000000;
    case 31 : return base_32_0000000000;
    case 32 : return base_33_0000000000;
    case 33 : return base_34_0000000000;
    case 34 : return base_35_0000000000;
    case 35 : return base_36_0000000000;
    case 36 : return base_37_0000000000;
    case 37 : return base_38_0000000000;
    case 38 : return base_39_0000000000;
    case 39 : return base_40_0000000000;
    case 40 : return base_41_0000000000;
    case 41 : return base_42_0000000000;
    case 42 : return base_43_0000000000;
    case 43 : return base_44_0000000000;
    case 44 : return base_45_0000000000;
    case 45 : return base_46_0000000000;
    case 46 : return base_47_0000000000;
    case 47 : return base_48_0000000000;
    case 48 : return base_49_0000000000;
    case 49 : return base_50_0000000000;
    case 50 : return base_51_0000000000;
    case 51 : return base_52_0000000000;
    case 52 : return base_53_0000000000;
    case 53 : return base_54_0000000000;
    case 54 : return base_55_0000000000;
    case 55 : return base_56_0000000000;
    case 56 : return base_57_0000000000;
    case 57 : return base_58_0000000000;
    case 58 : return base_59_0000000000;
    case 59 : return base_60_0000000000;
    case 60 : return base_61_0000000000;
    case 61 : return base_62_0000000000;
    case 62 : return base_63_0000000000;
    case 63 : return base_64_0000000000;
    default : return NULL;
  }
}
//---------------------------------------------------------------------------

Tp_adm_objetos *Ns_damas::Tp_damas::peca_de_madeira_do_indice(const int indx)
{
  switch (indx)
  {
    case 0 : return peca_jogador_01_01_0000000000;
    case 1 : return peca_jogador_01_02_0000000000;
    case 2 : return peca_jogador_01_03_0000000000;
    case 3 : return peca_jogador_01_04_0000000000;
    case 4 : return peca_jogador_01_05_0000000000;
    case 5 : return peca_jogador_01_06_0000000000;
    case 6 : return peca_jogador_01_07_0000000000;
    case 7 : return peca_jogador_01_08_0000000000;
    case 8 : return peca_jogador_01_09_0000000000;
    case 9 : return peca_jogador_01_10_0000000000;
    case 10 : return peca_jogador_01_11_0000000000;
    case 11 : return peca_jogador_01_12_0000000000;
    default : return NULL;
  }
}
//---------------------------------------------------------------------------

bool Ns_damas::Tp_damas::deletou_a_peca_que_estava_marcada_para_ser_deletada()
{
  Tp_adm_objetos *adm = NULL;

  for (int i = 0; i < TOTAL_DE_PECAS_POR_JOGADOR; i++)
  {
    adm = peca_branca_do_indice(i);
    if ((adm) && (adm -> foi_marcado_para_ser_deletado)){ deleta_a_peca(adm); return true;}
    adm = peca_de_madeira_do_indice(i);
    if ((adm) && (adm -> foi_marcado_para_ser_deletado)){ deleta_a_peca(adm); return true;}
  }

  return false;
}
//---------------------------------------------------------------------------

void Ns_damas::Tp_damas::posiciona_o_objeto(Tp_adm_objetos *adm, const bool sel)
{
  if (! (adm)) return;

  glPushMatrix();
    glTranslatef(adm -> x, adm -> y, adm -> z); adm -> desenha(sel);
  glPopMatrix();

  if (adm -> virou_dama)
  {
    glPushMatrix();
      glTranslatef(adm -> x, adm -> y, adm -> z + 5.0f); adm -> desenha(sel);
    glPopMatrix();
  }
}
//---------------------------------------------------------------------------

void Ns_damas::Tp_damas::touchesBegan(NSSet *touches, UIEvent *event, UIView *view)
{
  for (UITouch *t in touches)
  {
    CGPoint pt = [t locationInView:view];
    xtouch = pt.x; ytouch = pt.y;
  }
}
//---------------------------------------------------------------------------

void Ns_damas::Tp_damas::touchesMoved(NSSet *touches, UIEvent *event, UIView *view)
{
}
//---------------------------------------------------------------------------

void Ns_damas::Tp_damas::touchesEnded(NSSet *touches, UIEvent *event, UIView *view)
{
  for (UITouch *t in touches)
  {
    CGPoint pt = [t locationInView:view];
    xtouch = pt.x; ytouch = pt.y;

    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
      switch (retina)
      {
        case re_ipad_sem_retina :
          if ((pt.x < 75.0f) && (pt.y > -75.0f)) fim_de_jogo = true;
        break;
        case re_ipad_com_retina :
          if ((pt.x < 150.0f) && (pt.y > -150.0f)) fim_de_jogo = true;
        break;
        default : break;
      }
    }
    else
    {
      switch (retina)
      {
        case re_3_5 :
        case re_4_0 :
          if ((pt.x < 75.0f) && (pt.y > -75.0f)) fim_de_jogo = true;
        break;
        case re_null :
          if ((pt.x < 50.0f) && (pt.y > -50.0f)) fim_de_jogo = true;
        break;
        default : break;
      }
    }
  }

  xtouch = ytouch = -1;
}
//---------------------------------------------------------------------------

void Ns_damas::Tp_damas::desenha(const bool sel, const float larg, const float comp)
{
  retina = retina_display(larg, comp);

  if (! (sel))
  {
    luz_00 -> desenha();
    luz_01 -> desenha();
  }
  else
  {
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, -1);
    switch (retina)
    {
      case re_3_5 :
      case re_4_0 :
      case re_ipad_com_retina :
        glViewport(0, 0, (int)larg / 2, (int)comp / 2);
      break;
      default : break;
    }
  }

  if (! (sel)) textura[TEXTURA_DAMAS_00].set();
  glPushMatrix();
    glRotatef(rot_do_fundo, 0.0f, 0.0f, 1.0f);
    fundo_0000000000 -> desenha(sel);
  glPopMatrix();

  glPushMatrix();
    switch (jogador_da_vez)
    {
      case jv_branco :
        glTranslatef(0.0f, -110.0f, pss_da_seta_para_baixo);
        glRotatef(rot_da_seta_para_baixo, 0.0f, 0.0f, 1.0f);
      break;
      case jv_madeira :
        glTranslatef(0.0f, 110.0f, pss_da_seta_para_baixo);
        glRotatef(rot_da_seta_para_baixo, 0.0f, 0.0f, 1.0f);
      break;
      default : break;
    }
    seta_para_baixo_0000000000 -> desenha(sel);
  glPopMatrix();

  posiciona_o_objeto(base_03_0000000000, sel);
  posiciona_o_objeto(base_04_0000000000, sel);
  posiciona_o_objeto(base_05_0000000000, sel);
  posiciona_o_objeto(base_06_0000000000, sel);
  posiciona_o_objeto(base_07_0000000000, sel);
  posiciona_o_objeto(base_08_0000000000, sel);
  posiciona_o_objeto(base_09_0000000000, sel);
  posiciona_o_objeto(base_10_0000000000, sel);
  posiciona_o_objeto(base_11_0000000000, sel);
  posiciona_o_objeto(base_12_0000000000, sel);
  posiciona_o_objeto(base_13_0000000000, sel);
  posiciona_o_objeto(base_14_0000000000, sel);
  posiciona_o_objeto(base_15_0000000000, sel);
  posiciona_o_objeto(base_16_0000000000, sel);
  posiciona_o_objeto(base_17_0000000000, sel);
  posiciona_o_objeto(base_18_0000000000, sel);
  posiciona_o_objeto(base_19_0000000000, sel);
  posiciona_o_objeto(base_20_0000000000, sel);
  posiciona_o_objeto(base_21_0000000000, sel);
  posiciona_o_objeto(base_22_0000000000, sel);
  posiciona_o_objeto(base_23_0000000000, sel);
  posiciona_o_objeto(base_24_0000000000, sel);
  posiciona_o_objeto(base_25_0000000000, sel);
  posiciona_o_objeto(base_26_0000000000, sel);
  posiciona_o_objeto(base_27_0000000000, sel);
  posiciona_o_objeto(base_28_0000000000, sel);
  posiciona_o_objeto(base_29_0000000000, sel);
  posiciona_o_objeto(base_30_0000000000, sel);
  posiciona_o_objeto(base_31_0000000000, sel);
  posiciona_o_objeto(base_32_0000000000, sel);
  posiciona_o_objeto(base_33_0000000000, sel);
  posiciona_o_objeto(base_34_0000000000, sel);
  posiciona_o_objeto(base_35_0000000000, sel);
  posiciona_o_objeto(base_36_0000000000, sel);
  posiciona_o_objeto(base_37_0000000000, sel);
  posiciona_o_objeto(base_38_0000000000, sel);
  posiciona_o_objeto(base_39_0000000000, sel);
  posiciona_o_objeto(base_40_0000000000, sel);
  posiciona_o_objeto(base_41_0000000000, sel);
  posiciona_o_objeto(base_42_0000000000, sel);
  posiciona_o_objeto(base_43_0000000000, sel);
  posiciona_o_objeto(base_44_0000000000, sel);
  posiciona_o_objeto(base_45_0000000000, sel);
  posiciona_o_objeto(base_46_0000000000, sel);
  posiciona_o_objeto(base_47_0000000000, sel);
  posiciona_o_objeto(base_48_0000000000, sel);
  posiciona_o_objeto(base_49_0000000000, sel);
  posiciona_o_objeto(base_50_0000000000, sel);
  posiciona_o_objeto(base_51_0000000000, sel);
  posiciona_o_objeto(base_52_0000000000, sel);
  posiciona_o_objeto(base_53_0000000000, sel);
  posiciona_o_objeto(base_54_0000000000, sel);
  posiciona_o_objeto(base_55_0000000000, sel);
  posiciona_o_objeto(base_56_0000000000, sel);
  posiciona_o_objeto(base_57_0000000000, sel);
  posiciona_o_objeto(base_58_0000000000, sel);
  posiciona_o_objeto(base_59_0000000000, sel);
  posiciona_o_objeto(base_60_0000000000, sel);
  posiciona_o_objeto(base_61_0000000000, sel);
  posiciona_o_objeto(base_62_0000000000, sel);
  posiciona_o_objeto(base_63_0000000000, sel);
  posiciona_o_objeto(base_64_0000000000, sel);
  posiciona_o_objeto(base_02_0000000000, sel);
  posiciona_o_objeto(base_01_0000000000, sel);
  posiciona_o_objeto(peca_jogador_01_01_0000000000, sel);
  posiciona_o_objeto(peca_jogador_01_02_0000000000, sel);
  posiciona_o_objeto(peca_jogador_01_03_0000000000, sel);
  posiciona_o_objeto(peca_jogador_01_04_0000000000, sel);
  posiciona_o_objeto(peca_jogador_01_05_0000000000, sel);
  posiciona_o_objeto(peca_jogador_01_06_0000000000, sel);
  posiciona_o_objeto(peca_jogador_01_07_0000000000, sel);
  posiciona_o_objeto(peca_jogador_01_08_0000000000, sel);
  posiciona_o_objeto(peca_jogador_01_09_0000000000, sel);
  posiciona_o_objeto(peca_jogador_01_10_0000000000, sel);
  posiciona_o_objeto(peca_jogador_01_11_0000000000, sel);
  posiciona_o_objeto(peca_jogador_01_12_0000000000, sel);
  posiciona_o_objeto(peca_jogador_02_01_0000000000, sel);
  posiciona_o_objeto(peca_jogador_02_02_0000000000, sel);
  posiciona_o_objeto(peca_jogador_02_03_0000000000, sel);
  posiciona_o_objeto(peca_jogador_02_04_0000000000, sel);
  posiciona_o_objeto(peca_jogador_02_05_0000000000, sel);
  posiciona_o_objeto(peca_jogador_02_06_0000000000, sel);
  posiciona_o_objeto(peca_jogador_02_07_0000000000, sel);
  posiciona_o_objeto(peca_jogador_02_08_0000000000, sel);
  posiciona_o_objeto(peca_jogador_02_09_0000000000, sel);
  posiciona_o_objeto(peca_jogador_02_10_0000000000, sel);
  posiciona_o_objeto(peca_jogador_02_11_0000000000, sel);
  posiciona_o_objeto(peca_jogador_02_12_0000000000, sel);

  if (sel)
  {
    seleciona_objeto();
    glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    switch (retina)
    {
      case re_3_5 :
      case re_4_0 :
      case re_ipad_com_retina :
        glViewport(0, 0, (int)larg, (int)comp);
      break;
      default : break;
    }
  }
  else
  {
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
      camera -> desenha_3d(28.0f, larg / comp, 10.0f, 700.0f);
    else
      camera -> desenha_3d(25.0f, larg / comp, 10.0f, 700.0f);
  }
}
//---------------------------------------------------------------------------

Tp_adm_objetos *Ns_damas::Tp_damas::adm_objeto_do_indice(const int _xx, const int _yy)
{
  Tp_adm_objetos *adm = NULL;

  for (int i = 0; i < TOTAL_DE_PECAS_POR_JOGADOR; i++)
  {
    adm = peca_branca_do_indice(i);
    if ((adm) && (adm -> xx == _xx) && (adm -> yy == _yy)) return adm;
  }

  for (int i = 0; i < TOTAL_DE_PECAS_POR_JOGADOR; i++)
  {
    adm = peca_de_madeira_do_indice(i);
    if ((adm) && (adm -> xx == _xx) && (adm -> yy == _yy)) return adm;
  }

  return NULL;
}
//---------------------------------------------------------------------------

void Ns_damas::Tp_damas::desmarca_todas_as_pecas_que_estao_marcadas_para_ser_deletada()
{
  for (int i = 0; i < TOTAL_DE_PECAS_POR_JOGADOR; i++)
  {
    if (peca_branca_do_indice(i))
      peca_branca_do_indice(i) -> foi_marcado_para_ser_deletado = false;
    if (peca_de_madeira_do_indice(i))
      peca_de_madeira_do_indice(i) -> foi_marcado_para_ser_deletado = false;
  }
}
//---------------------------------------------------------------------------

void Ns_damas::Tp_damas::move_a_peca_para_o_local_selecionado(Tp_adm_objetos *peca_do_tabuleiro_sel)
{
  Tp_adm_objetos *adm = NULL;

  switch (jogador_da_vez)
  {
    case jv_branco :
      for (int i = 0; i < TOTAL_DE_PECAS_POR_JOGADOR; i++)
        if (peca_branca_do_indice(i))
          if (peca_branca_do_indice(i) -> foi_selecionado){ adm = peca_branca_do_indice(i); break;}
    break;
    case jv_madeira :
      for (int i = 0; i < TOTAL_DE_PECAS_POR_JOGADOR; i++)
        if (peca_de_madeira_do_indice(i))
          if (peca_de_madeira_do_indice(i) -> foi_selecionado){ adm = peca_de_madeira_do_indice(i); break;}
    break;
    default : break;
  }

  int xx = 0, yy = 0;
  for (int y = LIMITE_DO_TABULEIRO; y >= -LIMITE_DO_TABULEIRO; y-=PASSO_DO_TABULEIRO)
    for (int x = -LIMITE_DO_TABULEIRO; x <= LIMITE_DO_TABULEIRO; x+=PASSO_DO_TABULEIRO)
    {
      if ((tabuleiro[xx][yy].e_lugar_valido) &&
          (tabuleiro[xx][yy].adm_objeto -> foi_selecionado) && (tabuleiro[xx][yy].adm_objeto == peca_do_tabuleiro_sel))
      {
        tabuleiro[adm -> xx][adm -> yy].esta_ocupado = false;

        adm -> x = tabuleiro[xx][yy].x;
        adm -> y = tabuleiro[xx][yy].y;
        adm -> z = tabuleiro[xx][yy].z;
        adm -> xx = tabuleiro[xx][yy].xx;
        adm -> yy = tabuleiro[xx][yy].yy;

        tabuleiro[xx][yy].esta_ocupado = true;

        if ((adm -> y == -LIMITE_DO_TABULEIRO) && (adm -> jogador_da_vez == jv_branco))
          adm -> virou_dama = true;
        if ((adm -> y == LIMITE_DO_TABULEIRO) && (adm -> jogador_da_vez == jv_madeira))
          adm -> virou_dama = true;

        if (tabuleiro[xx][yy].deleta_a_peca_se_selecionado)
          if (deletou_a_peca_que_estava_marcada_para_ser_deletada())
            switch (jogador_da_vez)
            {
              case jv_madeira : if (--total_de_pecas_branca == 0) fim_de_jogo = true; break;
              case jv_branco : if (--total_de_pecas_de_madeira == 0) fim_de_jogo = true; break;
              default : break;
            }

        for (int i = 0; i < TOTAL_DE_PECAS_POR_JOGADOR; i++)
          if (peca_branca_do_indice(i))
            peca_branca_do_indice(i) -> foi_selecionado = false;
        for (int i = 0; i < TOTAL_DE_PECAS_POR_JOGADOR; i++)
          if (peca_de_madeira_do_indice(i))
            peca_de_madeira_do_indice(i) -> foi_selecionado = false;
        for (int i = 0; i < TOTAL_DE_BASES_EM_TODO_O_TABULEIRO; i++)
          peca_do_tabuleiro_indice(i) -> foi_selecionado = false;

        jogador_da_vez = (jogador_da_vez == jv_branco) ? jv_madeira : jv_branco;

        desmarca_todas_as_pecas_que_estao_marcadas_para_ser_deletada();
        desmarca_todas_as_pecas_do_tabuleiro_que_foram_marcadas_para_ser_deleta_se_selecionado();

        return;
      }

      xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
    }

  for (int i = 0; i < TOTAL_DE_PECAS_POR_JOGADOR; i++)
    if (peca_branca_do_indice(i))
      peca_branca_do_indice(i) -> foi_selecionado = false;
  for (int i = 0; i < TOTAL_DE_PECAS_POR_JOGADOR; i++)
    if (peca_de_madeira_do_indice(i))
      peca_de_madeira_do_indice(i) -> foi_selecionado = false;
  for (int i = 0; i < TOTAL_DE_BASES_EM_TODO_O_TABULEIRO; i++)
    peca_do_tabuleiro_indice(i) -> foi_selecionado = false;

  desmarca_todas_as_pecas_que_estao_marcadas_para_ser_deletada();
  desmarca_todas_as_pecas_do_tabuleiro_que_foram_marcadas_para_ser_deleta_se_selecionado();
}
//---------------------------------------------------------------------------

void Ns_damas::Tp_damas::selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(Tp_adm_objetos *adm)
{
  adm -> foi_selecionado = true;

  switch (jogador_da_vez)
  {
    case jv_branco :
      // avancar para comer peca em cima
      if ((adm -> y + PASSO_DO_TABULEIRO * 2 <= LIMITE_DO_TABULEIRO) &&
          (adm -> x + PASSO_DO_TABULEIRO * 2 <= LIMITE_DO_TABULEIRO))
        if ((! (tabuleiro[adm -> xx + 2][adm -> yy - 2].esta_ocupado)) && (tabuleiro[adm -> xx + 2][adm -> yy - 2].e_lugar_valido))
        {
          Tp_adm_objetos *_adm = adm_objeto_do_indice(tabuleiro[adm -> xx + 1][adm -> yy - 1].xx, tabuleiro[adm -> xx + 1][adm -> yy - 1].yy);
          if ((_adm) && (_adm -> jogador_da_vez != adm -> jogador_da_vez))
          {
            _adm -> foi_marcado_para_ser_deletado = true;
            tabuleiro[adm -> xx + 2][adm -> yy - 2].deleta_a_peca_se_selecionado = true;
            tabuleiro[adm -> xx + 2][adm -> yy - 2].adm_objeto -> foi_selecionado = true;
          }
        }
      if ((adm -> y + PASSO_DO_TABULEIRO * 2 <= LIMITE_DO_TABULEIRO) &&
          (adm -> x - PASSO_DO_TABULEIRO * 2 >= -LIMITE_DO_TABULEIRO))
        if ((! (tabuleiro[adm -> xx - 2][adm -> yy - 2].esta_ocupado)) && (tabuleiro[adm -> xx - 2][adm -> yy - 2].e_lugar_valido))
        {
          Tp_adm_objetos *_adm = adm_objeto_do_indice(tabuleiro[adm -> xx - 1][adm -> yy - 1].xx, tabuleiro[adm -> xx - 1][adm -> yy - 1].yy);
          if ((_adm) && (_adm -> jogador_da_vez != adm -> jogador_da_vez))
          {
            _adm -> foi_marcado_para_ser_deletado = true;
            tabuleiro[adm -> xx - 2][adm -> yy - 2].deleta_a_peca_se_selecionado = true;
            tabuleiro[adm -> xx - 2][adm -> yy - 2].adm_objeto -> foi_selecionado = true;
          }
        }

      // recuar para comer peca em baixo
      if ((adm -> y - PASSO_DO_TABULEIRO * 2 >= -LIMITE_DO_TABULEIRO) &&
          (adm -> x + PASSO_DO_TABULEIRO * 2 <= LIMITE_DO_TABULEIRO))
        if ((! (tabuleiro[adm -> xx + 2][adm -> yy + 2].esta_ocupado)) && (tabuleiro[adm -> xx + 2][adm -> yy + 2].e_lugar_valido))
        {
          Tp_adm_objetos *_adm = adm_objeto_do_indice(tabuleiro[adm -> xx + 1][adm -> yy + 1].xx, tabuleiro[adm -> xx + 1][adm -> yy + 1].yy);
          if ((_adm) && (_adm -> jogador_da_vez != adm -> jogador_da_vez))
          {
            _adm -> foi_marcado_para_ser_deletado = true;
            tabuleiro[adm -> xx + 2][adm -> yy + 2].deleta_a_peca_se_selecionado = true;
            tabuleiro[adm -> xx + 2][adm -> yy + 2].adm_objeto -> foi_selecionado = true;
          }
        }
      if ((adm -> y - PASSO_DO_TABULEIRO >= -LIMITE_DO_TABULEIRO) &&
          (adm -> x - PASSO_DO_TABULEIRO >= -LIMITE_DO_TABULEIRO))
        if ((! (tabuleiro[adm -> xx - 2][adm -> yy + 2].esta_ocupado)) && (tabuleiro[adm -> xx - 2][adm -> yy + 2].e_lugar_valido))
        {
          Tp_adm_objetos *_adm = adm_objeto_do_indice(tabuleiro[adm -> xx - 1][adm -> yy + 1].xx, tabuleiro[adm -> xx - 1][adm -> yy + 1].yy);
          if ((_adm) && (_adm -> jogador_da_vez != adm -> jogador_da_vez))
          {
            _adm -> foi_marcado_para_ser_deletado = true;
            tabuleiro[adm -> xx - 2][adm -> yy + 2].deleta_a_peca_se_selecionado = true;
            tabuleiro[adm -> xx - 2][adm -> yy + 2].adm_objeto -> foi_selecionado = true;
          }
        }

      // avancar sem comer peca
      if ((adm -> y + PASSO_DO_TABULEIRO <= LIMITE_DO_TABULEIRO) &&
          (adm -> x + PASSO_DO_TABULEIRO <= LIMITE_DO_TABULEIRO))
        if ((! (tabuleiro[adm -> xx + 1][adm -> yy - 1].esta_ocupado)) && (tabuleiro[adm -> xx + 1][adm -> yy - 1].e_lugar_valido))
          tabuleiro[adm -> xx + 1][adm -> yy - 1].adm_objeto -> foi_selecionado = true;
      if ((adm -> y + PASSO_DO_TABULEIRO <= LIMITE_DO_TABULEIRO) &&
          (adm -> x - PASSO_DO_TABULEIRO >= -LIMITE_DO_TABULEIRO))
        if ((! (tabuleiro[adm -> xx - 1][adm -> yy - 1].esta_ocupado)) && (tabuleiro[adm -> xx - 1][adm -> yy - 1].e_lugar_valido))
          tabuleiro[adm -> xx - 1][adm -> yy - 1].adm_objeto -> foi_selecionado = true;
    break;
    case jv_madeira :
      // recuar para comer peca em baixo
      if ((adm -> y - PASSO_DO_TABULEIRO * 2 >= -LIMITE_DO_TABULEIRO) &&
          (adm -> x + PASSO_DO_TABULEIRO * 2 <= LIMITE_DO_TABULEIRO))
        if ((! (tabuleiro[adm -> xx + 2][adm -> yy + 2].esta_ocupado)) && (tabuleiro[adm -> xx + 2][adm -> yy + 2].e_lugar_valido))
        {
          Tp_adm_objetos *_adm = adm_objeto_do_indice(tabuleiro[adm -> xx + 1][adm -> yy + 1].xx, tabuleiro[adm -> xx + 1][adm -> yy + 1].yy);
          if ((_adm) && (_adm -> jogador_da_vez != adm -> jogador_da_vez))
          {
            _adm -> foi_marcado_para_ser_deletado = true;
            tabuleiro[adm -> xx + 2][adm -> yy + 2].deleta_a_peca_se_selecionado = true;
            tabuleiro[adm -> xx + 2][adm -> yy + 2].adm_objeto -> foi_selecionado = true;
          }
        }
      if ((adm -> y - PASSO_DO_TABULEIRO >= -LIMITE_DO_TABULEIRO) &&
          (adm -> x - PASSO_DO_TABULEIRO >= -LIMITE_DO_TABULEIRO))
        if ((! (tabuleiro[adm -> xx - 2][adm -> yy + 2].esta_ocupado)) && (tabuleiro[adm -> xx - 2][adm -> yy + 2].e_lugar_valido))
        {
          Tp_adm_objetos *_adm = adm_objeto_do_indice(tabuleiro[adm -> xx - 1][adm -> yy + 1].xx, tabuleiro[adm -> xx - 1][adm -> yy + 1].yy);
          if ((_adm) && (_adm -> jogador_da_vez != adm -> jogador_da_vez))
          {
            _adm -> foi_marcado_para_ser_deletado = true;
            tabuleiro[adm -> xx - 2][adm -> yy + 2].deleta_a_peca_se_selecionado = true;
            tabuleiro[adm -> xx - 2][adm -> yy + 2].adm_objeto -> foi_selecionado = true;
          }
        }

      // avancar para comer peca em cima
      if ((adm -> y + PASSO_DO_TABULEIRO * 2 <= LIMITE_DO_TABULEIRO) &&
          (adm -> x + PASSO_DO_TABULEIRO * 2 <= LIMITE_DO_TABULEIRO))
        if ((! (tabuleiro[adm -> xx + 2][adm -> yy - 2].esta_ocupado)) && (tabuleiro[adm -> xx + 2][adm -> yy - 2].e_lugar_valido))
        {
          Tp_adm_objetos *_adm = adm_objeto_do_indice(tabuleiro[adm -> xx + 1][adm -> yy - 1].xx, tabuleiro[adm -> xx + 1][adm -> yy - 1].yy);
          if ((_adm) && (_adm -> jogador_da_vez != adm -> jogador_da_vez))
          {
            _adm -> foi_marcado_para_ser_deletado = true;
            tabuleiro[adm -> xx + 2][adm -> yy - 2].deleta_a_peca_se_selecionado = true;
            tabuleiro[adm -> xx + 2][adm -> yy - 2].adm_objeto -> foi_selecionado = true;
          }
        }
      if ((adm -> y + PASSO_DO_TABULEIRO * 2 <= LIMITE_DO_TABULEIRO) &&
          (adm -> x - PASSO_DO_TABULEIRO * 2 >= -LIMITE_DO_TABULEIRO))
        if ((! (tabuleiro[adm -> xx - 2][adm -> yy - 2].esta_ocupado)) && (tabuleiro[adm -> xx - 2][adm -> yy - 2].e_lugar_valido))
        {
          Tp_adm_objetos *_adm = adm_objeto_do_indice(tabuleiro[adm -> xx - 1][adm -> yy - 1].xx, tabuleiro[adm -> xx - 1][adm -> yy - 1].yy);
          if ((_adm) && (_adm -> jogador_da_vez != adm -> jogador_da_vez))
          {
            _adm -> foi_marcado_para_ser_deletado = true;
            tabuleiro[adm -> xx - 2][adm -> yy - 2].deleta_a_peca_se_selecionado = true;
            tabuleiro[adm -> xx - 2][adm -> yy - 2].adm_objeto -> foi_selecionado = true;
          }
        }

      // avancar sem comer peca
      if ((adm -> y - PASSO_DO_TABULEIRO >= -LIMITE_DO_TABULEIRO) &&
          (adm -> x + PASSO_DO_TABULEIRO <= LIMITE_DO_TABULEIRO))
        if ((! (tabuleiro[adm -> xx + 1][adm -> yy + 1].esta_ocupado)) && (tabuleiro[adm -> xx + 1][adm -> yy + 1].e_lugar_valido))
          tabuleiro[adm -> xx + 1][adm -> yy + 1].adm_objeto -> foi_selecionado = true;
      if ((adm -> y - PASSO_DO_TABULEIRO >= -LIMITE_DO_TABULEIRO) &&
          (adm -> x - PASSO_DO_TABULEIRO >= -LIMITE_DO_TABULEIRO))
        if ((! (tabuleiro[adm -> xx - 1][adm -> yy + 1].esta_ocupado)) && (tabuleiro[adm -> xx - 1][adm -> yy + 1].e_lugar_valido))
          tabuleiro[adm -> xx - 1][adm -> yy + 1].adm_objeto -> foi_selecionado = true;
    break;
    default : break;
  }
}
//---------------------------------------------------------------------------

void Ns_damas::Tp_damas::desmarca_todas_as_pecas_do_tabuleiro_que_foram_marcadas_para_ser_deleta_se_selecionado()
{
  int xx = 0, yy = 0;
  for (int y = LIMITE_DO_TABULEIRO; y >= -LIMITE_DO_TABULEIRO; y-=PASSO_DO_TABULEIRO)
    for (int x = -LIMITE_DO_TABULEIRO; x <= LIMITE_DO_TABULEIRO; x+=PASSO_DO_TABULEIRO)
    {
      tabuleiro[xx][yy].deleta_a_peca_se_selecionado = false;
      xx++; if (xx == TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO){ yy++; xx = 0;}
    }
}
//---------------------------------------------------------------------------

