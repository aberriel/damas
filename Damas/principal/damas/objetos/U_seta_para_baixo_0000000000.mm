//---------------------------------------------------------------------------

#include "U_damas.h"
//---------------------------------------------------------------------------
// funcoes auxiliares
//---------------------------------------------------------------------------

#pragma mark - funcoes auxiliares
#pragma mark -

void Ns_damas_seta_para_baixo_coortex_0000000000(unsigned int &id_coortex)
{
  float coortex[] = {0.7543f, 0.8758f, 0.9955f, 0.9964f, 0.9352f, 0.8758f,
                     0.7543f, 0.8758f, 0.9352f, 0.8758f, 0.9955f, 0.7552f,
                     0.9955f, 0.7552f, 0.9352f, 0.8758f, 0.9352f, 0.8758f,
                     0.9352f, 0.8758f, 0.9352f, 0.8758f, 0.9955f, 0.9964f,
                     0.9955f, 0.7552f, 0.7543f, 0.8758f, 0.9352f, 0.8758f,
                     0.9352f, 0.8758f, 0.7543f, 0.8758f, 0.9955f, 0.9964f};

  glGenBuffers(1, &id_coortex);
  glBindBuffer(GL_ARRAY_BUFFER, id_coortex);
  glBufferData(GL_ARRAY_BUFFER, 36 * sizeof(GL_FLOAT), coortex, GL_STATIC_DRAW);
}
//---------------------------------------------------------------------------

void Ns_damas_seta_para_baixo_normais_0000000000(unsigned int &id_normais)
{
  float normais[] = {0.4472f, -0.8944f, 0.0000f, 0.2182f, -0.4364f, -0.8729f, 0.4472f, -0.8944f, -0.0000f,
                     0.4472f, -0.8944f, 0.0000f, 0.4472f, -0.8944f, -0.0000f, 0.2182f, -0.4364f, 0.8729f,
                     -0.0000f, 0.8944f, 0.4472f, 0.0000f, 1.0000f, 0.0000f, 0.0000f, 1.0000f, 0.0000f,
                     0.0000f, 1.0000f, 0.0000f, 0.0000f, 1.0000f, 0.0000f, 0.0000f, 0.8944f, -0.4472f,
                     -0.2182f, -0.4364f, -0.8729f, -0.4472f, -0.8944f, 0.0000f, -0.4472f, -0.8944f, -0.0000f,
                     -0.4472f, -0.8944f, -0.0000f, -0.4472f, -0.8944f, 0.0000f, -0.2182f, -0.4364f, 0.8729f};

  glGenBuffers(1, &id_normais);
  glBindBuffer(GL_ARRAY_BUFFER, id_normais);
  glBufferData(GL_ARRAY_BUFFER, 54 * sizeof(GL_FLOAT), normais, GL_STATIC_DRAW);
}
//---------------------------------------------------------------------------

void Ns_damas_seta_para_baixo_vertice_0000000000(unsigned int &id_vertice)
{
  float vertice[] = {0.0000f, 0.0000f, -20.0000f, 0.0000f, 20.0000f, 20.0000f, 9.0000f, 0.0000f, 10.0000f,
                     0.0000f, 0.0000f, -20.0000f, 9.0000f, 0.0000f, 10.0000f, 0.0000f, -20.0000f, 20.0000f,
                     0.0000f, 20.0000f, 20.0000f, -9.0000f, 0.0000f, 10.0000f, 9.0000f, 0.0000f, 10.0000f,
                     9.0000f, 0.0000f, 10.0000f, -9.0000f, 0.0000f, 10.0000f, 0.0000f, -20.0000f, 20.0000f,
                     0.0000f, 20.0000f, 20.0000f, 0.0000f, 0.0000f, -20.0000f, -9.0000f, 0.0000f, 10.0000f,
                     -9.0000f, 0.0000f, 10.0000f, 0.0000f, 0.0000f, -20.0000f, 0.0000f, -20.0000f, 20.0000f};

  glGenBuffers(1, &id_vertice);
  glBindBuffer(GL_ARRAY_BUFFER, id_vertice);
  glBufferData(GL_ARRAY_BUFFER, 54 * sizeof(GL_FLOAT), vertice, GL_STATIC_DRAW);
}
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_seta_para_baixo_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_seta_para_baixo_0000000000
#pragma mark -

void Ns_damas::Tp_seta_para_baixo_0000000000::desenha(const bool sel)
{
  set_buffers(sel); glDrawArrays(GL_TRIANGLES, 0, 18);
}
//---------------------------------------------------------------------------

Ns_damas::Tp_seta_para_baixo_0000000000::~Tp_seta_para_baixo_0000000000()
{
}
//---------------------------------------------------------------------------

Ns_damas::Tp_seta_para_baixo_0000000000::Tp_seta_para_baixo_0000000000() : Tp_adm_objetos(0, 0, 0)
{
  nome = new char[16];
  strcpy(nome, "seta_para_baixo");
  Ns_damas_seta_para_baixo_coortex_0000000000(id_coortex);
  Ns_damas_seta_para_baixo_normais_0000000000(id_normais);
  Ns_damas_seta_para_baixo_vertice_0000000000(id_vertice);
  cor_solida[0] = 1.0000f; cor_solida[1] = 1.0000f; cor_solida[2] = 1.0000f; cor_solida[3] = 1.0000f;
}
//---------------------------------------------------------------------------

