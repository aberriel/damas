//---------------------------------------------------------------------------

#ifndef U_damasH
#define U_damasH
//---------------------------------------------------------------------------

#include "U_adm_objetos.h"
#include "U_damas_objetos.h"
//---------------------------------------------------------------------------
// namespace Ns_damas
//---------------------------------------------------------------------------

#pragma mark - namespace Ns_damas
#pragma mark -

namespace Ns_damas
{
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_01_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_01_0000000000
#pragma mark -

class Tp_base_01_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_01_0000000000();
  ~Tp_base_01_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_02_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_02_0000000000
#pragma mark -

class Tp_base_02_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_02_0000000000();
  ~Tp_base_02_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_03_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_03_0000000000
#pragma mark -

class Tp_base_03_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_03_0000000000();
  ~Tp_base_03_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_04_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_04_0000000000
#pragma mark -

class Tp_base_04_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_04_0000000000();
  ~Tp_base_04_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_05_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_05_0000000000
#pragma mark -

class Tp_base_05_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_05_0000000000();
  ~Tp_base_05_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_06_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_06_0000000000
#pragma mark -

class Tp_base_06_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_06_0000000000();
  ~Tp_base_06_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_07_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_07_0000000000
#pragma mark -

class Tp_base_07_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_07_0000000000();
  ~Tp_base_07_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_08_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_08_0000000000
#pragma mark -

class Tp_base_08_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_08_0000000000();
  ~Tp_base_08_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_09_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_09_0000000000
#pragma mark -

class Tp_base_09_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_09_0000000000();
  ~Tp_base_09_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_10_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_10_0000000000
#pragma mark -

class Tp_base_10_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_10_0000000000();
  ~Tp_base_10_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_11_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_11_0000000000
#pragma mark -

class Tp_base_11_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_11_0000000000();
  ~Tp_base_11_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_12_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_12_0000000000
#pragma mark -

class Tp_base_12_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_12_0000000000();
  ~Tp_base_12_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_13_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_13_0000000000
#pragma mark -

class Tp_base_13_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_13_0000000000();
  ~Tp_base_13_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_14_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_14_0000000000
#pragma mark -

class Tp_base_14_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_14_0000000000();
  ~Tp_base_14_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_15_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_15_0000000000
#pragma mark -

class Tp_base_15_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_15_0000000000();
  ~Tp_base_15_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_16_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_16_0000000000
#pragma mark -

class Tp_base_16_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_16_0000000000();
  ~Tp_base_16_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_17_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_17_0000000000
#pragma mark -

class Tp_base_17_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_17_0000000000();
  ~Tp_base_17_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_18_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_18_0000000000
#pragma mark -

class Tp_base_18_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_18_0000000000();
  ~Tp_base_18_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_19_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_19_0000000000
#pragma mark -

class Tp_base_19_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_19_0000000000();
  ~Tp_base_19_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_20_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_20_0000000000
#pragma mark -

class Tp_base_20_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_20_0000000000();
  ~Tp_base_20_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_21_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_21_0000000000
#pragma mark -

class Tp_base_21_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_21_0000000000();
  ~Tp_base_21_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_22_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_22_0000000000
#pragma mark -

class Tp_base_22_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_22_0000000000();
  ~Tp_base_22_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_23_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_23_0000000000
#pragma mark -

class Tp_base_23_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_23_0000000000();
  ~Tp_base_23_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_24_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_24_0000000000
#pragma mark -

class Tp_base_24_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_24_0000000000();
  ~Tp_base_24_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_25_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_25_0000000000
#pragma mark -

class Tp_base_25_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_25_0000000000();
  ~Tp_base_25_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_26_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_26_0000000000
#pragma mark -

class Tp_base_26_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_26_0000000000();
  ~Tp_base_26_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_27_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_27_0000000000
#pragma mark -

class Tp_base_27_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_27_0000000000();
  ~Tp_base_27_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_28_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_28_0000000000
#pragma mark -

class Tp_base_28_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_28_0000000000();
  ~Tp_base_28_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_29_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_29_0000000000
#pragma mark -

class Tp_base_29_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_29_0000000000();
  ~Tp_base_29_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_30_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_30_0000000000
#pragma mark -

class Tp_base_30_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_30_0000000000();
  ~Tp_base_30_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_31_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_31_0000000000
#pragma mark -

class Tp_base_31_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_31_0000000000();
  ~Tp_base_31_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_32_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_32_0000000000
#pragma mark -

class Tp_base_32_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_32_0000000000();
  ~Tp_base_32_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_33_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_33_0000000000
#pragma mark -

class Tp_base_33_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_33_0000000000();
  ~Tp_base_33_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_34_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_34_0000000000
#pragma mark -

class Tp_base_34_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_34_0000000000();
  ~Tp_base_34_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_35_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_35_0000000000
#pragma mark -

class Tp_base_35_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_35_0000000000();
  ~Tp_base_35_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_36_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_36_0000000000
#pragma mark -

class Tp_base_36_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_36_0000000000();
  ~Tp_base_36_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_37_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_37_0000000000
#pragma mark -

class Tp_base_37_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_37_0000000000();
  ~Tp_base_37_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_38_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_38_0000000000
#pragma mark -

class Tp_base_38_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_38_0000000000();
  ~Tp_base_38_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_39_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_39_0000000000
#pragma mark -

class Tp_base_39_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_39_0000000000();
  ~Tp_base_39_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_40_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_40_0000000000
#pragma mark -

class Tp_base_40_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_40_0000000000();
  ~Tp_base_40_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_41_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_41_0000000000
#pragma mark -

class Tp_base_41_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_41_0000000000();
  ~Tp_base_41_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_42_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_42_0000000000
#pragma mark -

class Tp_base_42_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_42_0000000000();
  ~Tp_base_42_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_43_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_43_0000000000
#pragma mark -

class Tp_base_43_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_43_0000000000();
  ~Tp_base_43_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_44_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_44_0000000000
#pragma mark -

class Tp_base_44_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_44_0000000000();
  ~Tp_base_44_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_45_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_45_0000000000
#pragma mark -

class Tp_base_45_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_45_0000000000();
  ~Tp_base_45_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_46_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_46_0000000000
#pragma mark -

class Tp_base_46_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_46_0000000000();
  ~Tp_base_46_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_47_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_47_0000000000
#pragma mark -

class Tp_base_47_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_47_0000000000();
  ~Tp_base_47_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_48_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_48_0000000000
#pragma mark -

class Tp_base_48_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_48_0000000000();
  ~Tp_base_48_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_49_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_49_0000000000
#pragma mark -

class Tp_base_49_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_49_0000000000();
  ~Tp_base_49_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_50_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_50_0000000000
#pragma mark -

class Tp_base_50_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_50_0000000000();
  ~Tp_base_50_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_51_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_51_0000000000
#pragma mark -

class Tp_base_51_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_51_0000000000();
  ~Tp_base_51_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_52_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_52_0000000000
#pragma mark -

class Tp_base_52_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_52_0000000000();
  ~Tp_base_52_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_53_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_53_0000000000
#pragma mark -

class Tp_base_53_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_53_0000000000();
  ~Tp_base_53_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_54_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_54_0000000000
#pragma mark -

class Tp_base_54_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_54_0000000000();
  ~Tp_base_54_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_55_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_55_0000000000
#pragma mark -

class Tp_base_55_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_55_0000000000();
  ~Tp_base_55_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_56_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_56_0000000000
#pragma mark -

class Tp_base_56_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_56_0000000000();
  ~Tp_base_56_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_57_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_57_0000000000
#pragma mark -

class Tp_base_57_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_57_0000000000();
  ~Tp_base_57_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_58_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_58_0000000000
#pragma mark -

class Tp_base_58_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_58_0000000000();
  ~Tp_base_58_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_59_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_59_0000000000
#pragma mark -

class Tp_base_59_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_59_0000000000();
  ~Tp_base_59_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_60_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_60_0000000000
#pragma mark -

class Tp_base_60_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_60_0000000000();
  ~Tp_base_60_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_61_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_61_0000000000
#pragma mark -

class Tp_base_61_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_61_0000000000();
  ~Tp_base_61_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_62_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_62_0000000000
#pragma mark -

class Tp_base_62_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_62_0000000000();
  ~Tp_base_62_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_63_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_63_0000000000
#pragma mark -

class Tp_base_63_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_63_0000000000();
  ~Tp_base_63_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_base_64_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_base_64_0000000000
#pragma mark -

class Tp_base_64_0000000000 : public Tp_adm_objetos
{
public :
  Tp_base_64_0000000000();
  ~Tp_base_64_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_fundo_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_fundo_0000000000
#pragma mark -

class Tp_fundo_0000000000 : public Tp_adm_objetos
{
public :
  Tp_fundo_0000000000();
  ~Tp_fundo_0000000000();
  void desenha(const bool sel);
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_01_01_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_01_01_0000000000
#pragma mark -

class Tp_peca_jogador_01_01_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_01_01_0000000000();
  ~Tp_peca_jogador_01_01_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_01_02_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_01_02_0000000000
#pragma mark -

class Tp_peca_jogador_01_02_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_01_02_0000000000();
  ~Tp_peca_jogador_01_02_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_01_03_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_01_03_0000000000
#pragma mark -

class Tp_peca_jogador_01_03_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_01_03_0000000000();
  ~Tp_peca_jogador_01_03_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_01_04_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_01_04_0000000000
#pragma mark -

class Tp_peca_jogador_01_04_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_01_04_0000000000();
  ~Tp_peca_jogador_01_04_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_01_05_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_01_05_0000000000
#pragma mark -

class Tp_peca_jogador_01_05_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_01_05_0000000000();
  ~Tp_peca_jogador_01_05_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_01_06_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_01_06_0000000000
#pragma mark -

class Tp_peca_jogador_01_06_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_01_06_0000000000();
  ~Tp_peca_jogador_01_06_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_01_07_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_01_07_0000000000
#pragma mark -

class Tp_peca_jogador_01_07_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_01_07_0000000000();
  ~Tp_peca_jogador_01_07_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_01_08_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_01_08_0000000000
#pragma mark -

class Tp_peca_jogador_01_08_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_01_08_0000000000();
  ~Tp_peca_jogador_01_08_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_01_09_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_01_09_0000000000
#pragma mark -

class Tp_peca_jogador_01_09_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_01_09_0000000000();
  ~Tp_peca_jogador_01_09_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_01_10_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_01_10_0000000000
#pragma mark -

class Tp_peca_jogador_01_10_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_01_10_0000000000();
  ~Tp_peca_jogador_01_10_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_01_11_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_01_11_0000000000
#pragma mark -

class Tp_peca_jogador_01_11_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_01_11_0000000000();
  ~Tp_peca_jogador_01_11_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_01_12_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_01_12_0000000000
#pragma mark -

class Tp_peca_jogador_01_12_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_01_12_0000000000();
  ~Tp_peca_jogador_01_12_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_02_01_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_02_01_0000000000
#pragma mark -

class Tp_peca_jogador_02_01_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_02_01_0000000000();
  ~Tp_peca_jogador_02_01_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_02_02_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_02_02_0000000000
#pragma mark -

class Tp_peca_jogador_02_02_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_02_02_0000000000();
  ~Tp_peca_jogador_02_02_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_02_03_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_02_03_0000000000
#pragma mark -

class Tp_peca_jogador_02_03_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_02_03_0000000000();
  ~Tp_peca_jogador_02_03_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_02_04_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_02_04_0000000000
#pragma mark -

class Tp_peca_jogador_02_04_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_02_04_0000000000();
  ~Tp_peca_jogador_02_04_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_02_05_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_02_05_0000000000
#pragma mark -

class Tp_peca_jogador_02_05_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_02_05_0000000000();
  ~Tp_peca_jogador_02_05_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_02_06_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_02_06_0000000000
#pragma mark -

class Tp_peca_jogador_02_06_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_02_06_0000000000();
  ~Tp_peca_jogador_02_06_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_02_07_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_02_07_0000000000
#pragma mark -

class Tp_peca_jogador_02_07_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_02_07_0000000000();
  ~Tp_peca_jogador_02_07_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_02_08_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_02_08_0000000000
#pragma mark -

class Tp_peca_jogador_02_08_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_02_08_0000000000();
  ~Tp_peca_jogador_02_08_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_02_09_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_02_09_0000000000
#pragma mark -

class Tp_peca_jogador_02_09_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_02_09_0000000000();
  ~Tp_peca_jogador_02_09_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_02_10_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_02_10_0000000000
#pragma mark -

class Tp_peca_jogador_02_10_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_02_10_0000000000();
  ~Tp_peca_jogador_02_10_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_02_11_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_02_11_0000000000
#pragma mark -

class Tp_peca_jogador_02_11_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_02_11_0000000000();
  ~Tp_peca_jogador_02_11_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_peca_jogador_02_12_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_peca_jogador_02_12_0000000000
#pragma mark -

class Tp_peca_jogador_02_12_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_peca_jogador_02_12_0000000000();
  ~Tp_peca_jogador_02_12_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_seta_para_baixo_0000000000
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_seta_para_baixo_0000000000
#pragma mark -

class Tp_seta_para_baixo_0000000000 : public Tp_adm_objetos
{
public :
  void desenha(const bool sel);
  Tp_seta_para_baixo_0000000000();
  ~Tp_seta_para_baixo_0000000000();
};
//---------------------------------------------------------------------------
// classe Ns_damas::Tp_damas
//---------------------------------------------------------------------------

#pragma mark - classe Ns_damas::Tp_damas
#pragma mark -

class Tp_damas
{
private :
  Tp_luz *luz_00;
  Tp_luz *luz_01;
  Tp_retina retina;
  Tp_camera *camera;
  float rot_do_fundo;
  Tp_textura *textura;
  int total_de_pecas_branca;
  bool seta_para_baixo_sobe;
  float rot_da_seta_para_baixo;
  float pss_da_seta_para_baixo;
  int total_de_pecas_de_madeira;
  Tp_jogador_da_vez jogador_da_vez;
  Tp_dados_do_tabuleiro tabuleiro[TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO][TOTAL_DE_BASES_POR_LADO_DO_TABULEIRO];

  void seleciona_objeto();
  bool existe_uma_peca_selecionada();
  void deleta_a_peca(Tp_adm_objetos *adm);
  Tp_adm_objetos *peca_branca_do_indice(const int indx);
  Tp_adm_objetos *peca_do_tabuleiro_indice(const int indx);
  Tp_adm_objetos *peca_de_madeira_do_indice(const int indx);
  bool deletou_a_peca_que_estava_marcada_para_ser_deletada();
  void posiciona_o_objeto(Tp_adm_objetos *adm, const bool sel);
  Tp_adm_objetos *adm_objeto_do_indice(const int _xx, const int _yy);
  void desmarca_todas_as_pecas_que_estao_marcadas_para_ser_deletada();
  void move_a_peca_para_o_local_selecionado(Tp_adm_objetos *peca_do_tabuleiro_sel);
  void selecione_as_pecas_do_tabuleiro_aonde_e_possivel_jogar(Tp_adm_objetos *adm);
  void desmarca_todas_as_pecas_do_tabuleiro_que_foram_marcadas_para_ser_deleta_se_selecionado();
public :
  bool fim_de_jogo;
  Tp_fundo_0000000000 *fundo_0000000000;
  Tp_base_03_0000000000 *base_03_0000000000;
  Tp_base_04_0000000000 *base_04_0000000000;
  Tp_base_05_0000000000 *base_05_0000000000;
  Tp_base_06_0000000000 *base_06_0000000000;
  Tp_base_07_0000000000 *base_07_0000000000;
  Tp_base_08_0000000000 *base_08_0000000000;
  Tp_base_09_0000000000 *base_09_0000000000;
  Tp_base_10_0000000000 *base_10_0000000000;
  Tp_base_11_0000000000 *base_11_0000000000;
  Tp_base_12_0000000000 *base_12_0000000000;
  Tp_base_13_0000000000 *base_13_0000000000;
  Tp_base_14_0000000000 *base_14_0000000000;
  Tp_base_15_0000000000 *base_15_0000000000;
  Tp_base_16_0000000000 *base_16_0000000000;
  Tp_base_17_0000000000 *base_17_0000000000;
  Tp_base_18_0000000000 *base_18_0000000000;
  Tp_base_19_0000000000 *base_19_0000000000;
  Tp_base_20_0000000000 *base_20_0000000000;
  Tp_base_21_0000000000 *base_21_0000000000;
  Tp_base_22_0000000000 *base_22_0000000000;
  Tp_base_23_0000000000 *base_23_0000000000;
  Tp_base_24_0000000000 *base_24_0000000000;
  Tp_base_25_0000000000 *base_25_0000000000;
  Tp_base_26_0000000000 *base_26_0000000000;
  Tp_base_27_0000000000 *base_27_0000000000;
  Tp_base_28_0000000000 *base_28_0000000000;
  Tp_base_29_0000000000 *base_29_0000000000;
  Tp_base_30_0000000000 *base_30_0000000000;
  Tp_base_31_0000000000 *base_31_0000000000;
  Tp_base_32_0000000000 *base_32_0000000000;
  Tp_base_33_0000000000 *base_33_0000000000;
  Tp_base_34_0000000000 *base_34_0000000000;
  Tp_base_35_0000000000 *base_35_0000000000;
  Tp_base_36_0000000000 *base_36_0000000000;
  Tp_base_37_0000000000 *base_37_0000000000;
  Tp_base_38_0000000000 *base_38_0000000000;
  Tp_base_39_0000000000 *base_39_0000000000;
  Tp_base_40_0000000000 *base_40_0000000000;
  Tp_base_41_0000000000 *base_41_0000000000;
  Tp_base_42_0000000000 *base_42_0000000000;
  Tp_base_43_0000000000 *base_43_0000000000;
  Tp_base_44_0000000000 *base_44_0000000000;
  Tp_base_45_0000000000 *base_45_0000000000;
  Tp_base_46_0000000000 *base_46_0000000000;
  Tp_base_47_0000000000 *base_47_0000000000;
  Tp_base_48_0000000000 *base_48_0000000000;
  Tp_base_49_0000000000 *base_49_0000000000;
  Tp_base_50_0000000000 *base_50_0000000000;
  Tp_base_51_0000000000 *base_51_0000000000;
  Tp_base_52_0000000000 *base_52_0000000000;
  Tp_base_53_0000000000 *base_53_0000000000;
  Tp_base_54_0000000000 *base_54_0000000000;
  Tp_base_55_0000000000 *base_55_0000000000;
  Tp_base_56_0000000000 *base_56_0000000000;
  Tp_base_57_0000000000 *base_57_0000000000;
  Tp_base_58_0000000000 *base_58_0000000000;
  Tp_base_59_0000000000 *base_59_0000000000;
  Tp_base_60_0000000000 *base_60_0000000000;
  Tp_base_61_0000000000 *base_61_0000000000;
  Tp_base_62_0000000000 *base_62_0000000000;
  Tp_base_63_0000000000 *base_63_0000000000;
  Tp_base_64_0000000000 *base_64_0000000000;
  Tp_base_02_0000000000 *base_02_0000000000;
  Tp_base_01_0000000000 *base_01_0000000000;
  Tp_seta_para_baixo_0000000000 *seta_para_baixo_0000000000;
  Tp_peca_jogador_01_01_0000000000 *peca_jogador_01_01_0000000000;
  Tp_peca_jogador_01_02_0000000000 *peca_jogador_01_02_0000000000;
  Tp_peca_jogador_01_03_0000000000 *peca_jogador_01_03_0000000000;
  Tp_peca_jogador_01_04_0000000000 *peca_jogador_01_04_0000000000;
  Tp_peca_jogador_01_05_0000000000 *peca_jogador_01_05_0000000000;
  Tp_peca_jogador_01_06_0000000000 *peca_jogador_01_06_0000000000;
  Tp_peca_jogador_01_07_0000000000 *peca_jogador_01_07_0000000000;
  Tp_peca_jogador_01_08_0000000000 *peca_jogador_01_08_0000000000;
  Tp_peca_jogador_01_09_0000000000 *peca_jogador_01_09_0000000000;
  Tp_peca_jogador_01_10_0000000000 *peca_jogador_01_10_0000000000;
  Tp_peca_jogador_01_11_0000000000 *peca_jogador_01_11_0000000000;
  Tp_peca_jogador_01_12_0000000000 *peca_jogador_01_12_0000000000;
  Tp_peca_jogador_02_01_0000000000 *peca_jogador_02_01_0000000000;
  Tp_peca_jogador_02_02_0000000000 *peca_jogador_02_02_0000000000;
  Tp_peca_jogador_02_03_0000000000 *peca_jogador_02_03_0000000000;
  Tp_peca_jogador_02_04_0000000000 *peca_jogador_02_04_0000000000;
  Tp_peca_jogador_02_05_0000000000 *peca_jogador_02_05_0000000000;
  Tp_peca_jogador_02_06_0000000000 *peca_jogador_02_06_0000000000;
  Tp_peca_jogador_02_07_0000000000 *peca_jogador_02_07_0000000000;
  Tp_peca_jogador_02_08_0000000000 *peca_jogador_02_08_0000000000;
  Tp_peca_jogador_02_09_0000000000 *peca_jogador_02_09_0000000000;
  Tp_peca_jogador_02_10_0000000000 *peca_jogador_02_10_0000000000;
  Tp_peca_jogador_02_11_0000000000 *peca_jogador_02_11_0000000000;
  Tp_peca_jogador_02_12_0000000000 *peca_jogador_02_12_0000000000;

  Tp_damas();
  ~Tp_damas();
  void trabalha();
  void touchesBegan(NSSet *touches, UIEvent *event, UIView *view);
  void touchesMoved(NSSet *touches, UIEvent *event, UIView *view);
  void touchesEnded(NSSet *touches, UIEvent *event, UIView *view);
  void desenha(const bool sel, const float larg, const float comp);
};
//---------------------------------------------------------------------------

}; // namespace Ns_damas
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------

